from .admin import mod as admin
from .gallery import mod as gallery
from .pages import mod as pages
from .panoramas import mod as panoramas
