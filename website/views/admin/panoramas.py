# coding: utf-8
import os
from flask import jsonify, request
from flask.views import MethodView
from website.database import Panorama, db
from website.thumbnailer import create_thumbnail
from website.views.admin import login_required



class PanoramasAPI(MethodView):
    decorators = [login_required]

    def get(self, pano_id):
        if pano_id is None:
            panoramas = Panorama.query.order_by(Panorama.date_added)
            return jsonify(success=True,
                           panoramas=[p.to_json() for p in panoramas])
        else:
            panorama = Panorama.query.get_or_404(pano_id)
            return jsonify(success=True,
                           panorama=panorama.to_json())

    def post(self):
        name = request.json.get('name')
        if not name:
            return jsonify(success=False, message=u'Нам надо какое нибудь имя')

        panorama = Panorama(name=name)
        db.session.add(panorama)
        db.session.commit()

        os.makedirs(panorama.upload_path)

        return jsonify(success=True, panorama=panorama.to_json())

    def put(self, pano_id):
        panorama = Panorama.query.get_or_404(pano_id)

        if request.files:
            return self.upload_file(panorama)
        else:
            return self.update_params(panorama)

    def delete(self, pano_id):
        panorama = Panorama.query.get_or_404(pano_id)

        if panorama.published:
            return jsonify(success=False, message=u'Нельзя удалять '
                                                  u'опубликованные панорамы')

        db.session.delete(panorama)
        db.session.commit()

        return jsonify(success=True, panorama=panorama.to_json())

    def upload_file(self, panorama):
        upload_type, data = request.files.items()[0]
        if upload_type not in Panorama.FILE_NAMES:
            return jsonify(success=False, message=u'Неизвестный тип файлов')

        filename, field = Panorama.FILE_NAMES.get(upload_type.lower())
        path = os.path.join(panorama.upload_path, filename)

        try:
            if os.path.exists(path):
                os.remove(path)

            data.save(path)
        except Exception as error:
            return jsonify(success=False, message=str(error))

        if upload_type == 'image':
            create_thumbnail(path, '435x200')
            panorama.has_preview = True
        else:
            setattr(panorama, field, os.path.getsize(path))

        db.session.commit()

        return jsonify(success=True, panorama=panorama.to_json())

    def update_params(self, panorama):
        if 'name' in request.json:
            panorama.name = request.json['name']
        if 'published' in request.json:
            panorama.published = request.json['published']

        db.session.commit()

        return jsonify(success=True, panorama=panorama.to_json())
