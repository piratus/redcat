# coding: utf-8
from flask import jsonify, json, request
from flask.views import MethodView
from website import db
from website.database import Page
from website.views.admin import login_required


class PagesAPI(MethodView):
    decorators = [login_required]

    def get(self, page_id):
        """Gets Page info by 'page_id'.
        If 'page_id' is None returns a list of all pages.
        If a page is not found, returns 404.
        """
        if page_id is None:
            pages = Page.query.order_by(Page.order)
            return jsonify(success=True,
                           pages=[page.to_json() for page in pages])
        else:
            page = Page.query.get_or_404(page_id)
            return jsonify(success=True, page=page.to_json())

    def post(self):
        """Creates a new page. New page is never published."""
        data = request.json or request.form
        if not 'title' in data:
            return jsonify(success=False, msg=u'Без заголовка нельзя')

        try:
            page = update_page(Page(), data, exclude=['published'])
        except AttributeError:
            return jsonify(success=False, msg=u'Неправильные параметры запроса')

        db.session.add(page)
        db.session.commit()
        return jsonify(success=True, page=page.to_json())

    def put(self, page_id):
        """Updates an existing page.
        'published' can be set to True only if the page has 'title',
        'menu_title', and 'slug'.
        """
        data = request.json or request.form
        page = Page.query.get_or_404(page_id)
        try:
            update_page(page, data, exclude=['published'])
        except AttributeError:
            return jsonify(success=False, msg=u'Неправильные параметры запроса')

        if 'published' in data:
            if not page.title or not page.menu_title or not page.slug:
                return jsonify(success=False,
                               msg=u'Перед публикацией страницы'
                                   u' нужно задать заголовок и ссылку.')
            else:
                page.published = data['published']

        db.session.commit()
        return jsonify(success=True, page=page.to_json())

    def delete(self, page_id):
        """Deletes a page. Only if a page is not published."""
        page = Page.query.get_or_404(page_id)
        if page.published:
            return jsonify(success=False,
                           msg=u'Нельзя удалять опубликованную страницу.')

        db.session.delete(page)
        db.session.commit()
        return jsonify(success=True, page=page.to_json())


@login_required
def update_page(page, data, exclude=None):
    """Update object params from a dict
    `excluded` is a list of keys to be skipped.
    """
    exclude = exclude or []
    for key, value in data.iteritems():
        if key not in exclude:
            setattr(page, key, value)

    return page


@login_required
def update_page_order():
    data = [json.loads(item) for item in request.form.getlist('items[]')]
    try:
        for item in data:
            params = {'order': item['order'], 'parent_id': item['parent_id']}
            Page.query.filter(Page.id == item['id']).update(params)
    except KeyError as error:
        return jsonify(success=False, msg=error)

    db.session.commit()
    return jsonify(success=True)
