# coding: utf-8
from functools import wraps
from flask import session, request, redirect, url_for, render_template, flash
from website.settings import ADMINS


def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not session.get('logged_in', False):
            if request.is_xhr:
                return 'Not authorised', 401
            return redirect(url_for('admin.login'))
        return func(*args, **kwargs)

    return wrapper


def login():
    """A mock for login"""
    users = {name: password for name, password in ADMINS}
    if request.method == 'POST':
        name = request.form.get('username', None)
        password = request.form.get('password', None)
        if not (name and name in users and users[name] == password):
            flash(u'Неправильное имя или пароль')
            return redirect(url_for('admin.login'))
        session['logged_in'] = True

    if session.get('logged_in', False):
        return redirect(url_for('admin.index'))

    return render_template('admin/login.html')


def logout():
    """A mock logout"""
    session.pop('logged_in', None)
    return redirect(url_for('admin.login'))
