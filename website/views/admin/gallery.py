# coding: utf-8
from base64 import b64decode
import os
from random import choice
from string import lowercase
from flask import jsonify, request
from flask.views import MethodView
from pytils.translit import slugify

from website.database import Album, db, Photo
from website.settings import PHOTO_PATH
from website.thumbnailer import create_thumbnail
from website.utils import extend_filename
from .auth import login_required


class AlbumAPI(MethodView):
    """CRUD operations for albums"""
    decorators = [login_required]

    def get(self, album_id):
        """List all albums if album_id is None or display detailed
        album info
        """
        if album_id is None:
            albums = Album.query.order_by(Album.order).all()
            return jsonify(success=True,
                           albums=[album.to_json() for album in albums])
        else:
            album = Album.query.get_or_404(album_id)
            return jsonify(
                success=True,
                album=album.to_json(),
                photos=[photo.to_json() for photo in album.photos]
            )

    def post(self):
        """Create new album"""
        data = request.json or request.form
        name = data['name']
        if not name:
            return jsonify(success=False, msg=u'Не бывает альбомов без имени')

        slug = data.get('slug', '')
        if slug == '':
            slug = slugify(name)
        if Album.query.filter((Album.name == name) | (Album.slug == slug)).count():
            return jsonify(success=False, msg=u'Такой альбом уже есть')

        album = Album(name=name, slug=slug, order=data.get('order', 0))
        db.session.add(album)
        db.session.commit()

        return jsonify(success=True, album=album.to_json())

    def put(self, album_id):
        """Update album"""
        data = request.json or request.form
        album = Album.query.get_or_404(album_id)
        album.name = data.get('name', album.name)
        album.slug = data.get('slug', album.slug)
        album.order = data.get('order', album.order)
        db.session.commit()
        return jsonify(success=True, album=album.to_json())

    def delete(self, album_id):
        """Delete album"""
        album = Album.query.get_or_404(album_id)
        db.session.delete(album)
        db.session.commit()
        return jsonify(success=True, album=album.to_json())


@login_required
def update_album_order():
    for album_id, order in request.form.iteritems():
        Album.query.filter(Album.id == album_id).update({'order': order})
    db.session.commit()
    return jsonify(success=True)


class PhotoAPI(MethodView):
    """CRUD operations for photos"""
    decorators = [login_required]

    def get(self, photo_id):
        """Photo deatils"""
        photo = Photo.query.get_or_404(photo_id)
        return jsonify(success=True, photo=photo.to_json())

    def post(self):
        """Upload a photo"""
        album_id = request.form.get('album_id', -1)
        album = Album.query.get_or_404(album_id)

        try:
            data = request.files.get('photo')
            filename = data.filename
        except Exception as error:
            print error


        if not filename.lower().endswith('.jpg'):
            return jsonify(success=False,
                           msg=u'Только jpg файлы, пожалуйста')

        while os.path.exists(os.path.join(PHOTO_PATH, filename)):
            filename = extend_filename(filename, choice(lowercase))


        with open(os.path.join(PHOTO_PATH, filename), 'wb') as image:
            data.save(image)

        photo = Photo(album=album, path=filename)
        db.session.add(photo)
        db.session.commit()

        create_thumbnail(os.path.join(PHOTO_PATH, filename))

        return jsonify(success=True, photo=photo.to_json())

    def delete(self, photo_id):
        """Delete a photo"""
        photo = Photo.query.get_or_404(photo_id)
        db.session.delete(photo)
        _cleanup_files(photo)
        db.session.commit()
        return jsonify(success=True, photo=photo.to_json())


@login_required
def update_photo_order(album_id):
    for photo_id, order in request.form.iteritems():
        Photo.query.filter(Photo.id == photo_id).update({'order': order})
    db.session.commit()
    return jsonify(success=True)


def _cleanup_files(photo):
    """Deletes photo and thumbnails"""
    photo_path = os.path.join(PHOTO_PATH, photo.path)
    if os.path.exists(photo_path):
        #FIXME: Remove thumbnails
        os.remove(photo_path)
