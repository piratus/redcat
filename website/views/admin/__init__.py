# coding: utf-8
""" Admin views for redcatstudio """

from flask import Blueprint, render_template

from .auth import login_required, login, logout
from .gallery import AlbumAPI, update_album_order, PhotoAPI, update_photo_order
from .pages import PagesAPI, update_page_order
from .panoramas import PanoramasAPI

mod = Blueprint('admin', __name__, url_prefix='/admin', static_folder='static')


@login_required
def index():
    return render_template('admin/index.html')

mod.add_url_rule('/', view_func=index, methods=['GET'])
mod.add_url_rule('/login/', view_func=login, methods=['GET', 'POST'])
mod.add_url_rule('/logout/', view_func=logout, methods=['GET'])


albums_view = AlbumAPI.as_view('album_api')
mod.add_url_rule('/api/albums/', defaults={'album_id': None},
                 view_func=albums_view, methods=['GET'])
mod.add_url_rule('/api/albums/', view_func=albums_view, methods=['POST'])
mod.add_url_rule('/api/albums/<int:album_id>/', view_func=albums_view,
                 methods=['GET', 'PUT', 'DELETE'])
mod.add_url_rule('/api/albums/update_order/', view_func=update_album_order,
                 methods=['POST'])


photos_view = PhotoAPI.as_view('photo_api')
mod.add_url_rule('/api/photos/', view_func=photos_view, methods=['POST'])
mod.add_url_rule('/api/photos/<int:photo_id>/', view_func=photos_view,
                 methods=['GET', 'DELETE'])
mod.add_url_rule('/api/albums/<int:album_id>/update_order/',
                 view_func=update_photo_order, methods=['POST'])


panoramas_view = PanoramasAPI.as_view('panoramas_api')
mod.add_url_rule('/api/panoramas/', view_func=panoramas_view,
                 methods=['GET'], defaults={'pano_id': None})
mod.add_url_rule('/api/panoramas/', view_func=panoramas_view, methods=['POST'])
mod.add_url_rule('/api/panoramas/<int:pano_id>/', view_func=panoramas_view,
                 methods=['GET', 'PUT', 'DELETE'])


pages_view = PagesAPI.as_view('pages_api')
mod.add_url_rule('/api/pages/', view_func=pages_view,
                 methods=['GET'], defaults={'page_id': None})
mod.add_url_rule('/api/pages/', view_func=pages_view, methods=['POST'])
mod.add_url_rule('/api/pages/<int:page_id>/', view_func=pages_view,
                 methods=['GET', 'PUT', 'DELETE'])
mod.add_url_rule('/api/pages/update_order/', view_func=update_page_order,
                 methods=['POST'])

