# coding: utf-8
import os

from flask import (Blueprint, render_template, request, abort,
                   send_from_directory)
from website.database import Album
from website.settings import PHOTO_PATH
from website.thumbnailer import create_thumbnail
from website.utils import extend_filename


mod = Blueprint('gallery', __name__,
                url_prefix='/albums', static_folder='static')


@mod.route('/')
def list_albums():
    albums = Album.query.order_by(Album.order).all()
    return render_template('albums_list.html', albums=albums,
                           selected='gallery')


@mod.route('/<slug>/')
def album(slug):
    #FIXME: optimize
    album_obj = Album.query.filter(Album.slug == slug).first()
    albums = Album.query.order_by(Album.order).all()
    if album_obj is None:
        abort(404)
    return render_template('album_detail.html',
                           album=album_obj, albums=albums, selected='gallery')


@mod.route('/photos/<filename>')
def show_photo(filename):
    size = request.args.get('size', False)
    crop = request.args.get('crop', True)
    if size:
        new_filename = extend_filename(filename, size)
        if not os.path.exists(os.path.join(PHOTO_PATH, new_filename)):
            create_thumbnail(os.path.join(PHOTO_PATH, filename), size, crop)
        filename = new_filename

    return send_from_directory(PHOTO_PATH, filename)
