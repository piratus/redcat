# coding: utf-8
from flask import Blueprint, render_template, request
from flask.helpers import url_for
from website.database import Panorama


mod = Blueprint('panoramas', __name__,
                url_prefix='/panoramas', static_folder='static')


@mod.route('/')
def list_panoramas():
    panoramas = Panorama.query\
        .order_by(Panorama.date_added)\
        .filter_by(published=True)

    return render_template('pano_list.html', panoramas=panoramas,
                           selected='panoramas')


@mod.route('/<int:pano_id>')
def view_panorama(pano_id):
    panorama = Panorama.query\
        .filter_by(published=True, id=pano_id)\
        .first_or_404()

    quality = request.args.get('quality')
    if not quality in {'mq', 'hq'}:
        quality = 'mq'
    filename = url_for('static', filename=panorama.file_for(quality))
    return render_template('pano_detail.html', panorama=panorama,
                           filename=filename, selected='panoramas')
