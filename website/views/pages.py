# coding: utf-8
from flask import (Blueprint, render_template, abort, request, redirect, flash,
                   url_for)
from jinja2 import nodes
from jinja2.ext import Extension
from sqlalchemy.orm import aliased
from website.database import db, Message, Page, PAGES_TEMPLATES


mod = Blueprint('pages', __name__, url_prefix='')


@mod.route('/<path:slug>/')
def view_page(slug):
    page = Page.query.filter(Page.slug == slug).first()
    if not page:
        abort(404)
    template = PAGES_TEMPLATES.get(page.template, 'pages/basic.html')
    return render_template(template, page=page)


@mod.route('/<slug>/', methods=['POST'])
def post_message(slug):
    data = request.form
    fields = ['name', 'email', 'message']
    if any(len(data.get(field, '')) == 0 for field in fields):
        flash(u'Нужно заполнить все поля формы.', 'error')
    else:
        db.session.add(Message(data['name'], data['email'], data['message']))
        db.session.commit()
        flash(u'Ваше сообщение успешно отправлено.')
    return redirect(request.headers.get('Referer', '/%s/' % slug))


class MenuExtension(Extension):
    tags = {'menu', 'submenu'}
    endtags = {'name:endmenu', 'name:endsubmenu'}

    def parse(self, parser):
        method_name = '_create_{0}'.format(parser.stream.current.value)
        lineno = parser.stream.next().lineno
        body = parser.parse_statements(self.endtags, drop_needle=True)
        params = [
            nodes.Name('title', 'store'),
            nodes.Name('url', 'store'),
            nodes.Name('selected', 'store')
        ]
        method = self.call_method(method_name, args=[])
        return nodes.CallBlock(method, params, [], body).set_lineno(lineno)

    def _create_menu(self, caller):
        gallery_path = url_for('gallery.list_albums')
        panoramas_path = url_for('panoramas.list_panoramas')
        result = (
            caller(u'Галлерея', gallery_path, gallery_path == request.path) +
            caller(u'Панорамы', panoramas_path, panoramas_path == request.path)
        )
        for page in Page.query.order_by(Page.order)\
                .filter(Page.published == True)\
                .filter(Page.parent == None):
            gallery_path = url_for('pages.view_page', slug=page.slug)
            result += caller(page.menu_title, gallery_path, request.path == gallery_path)
        return result

    def _create_submenu(self, caller):
        slug = str(request.path[1:-1].split('/')[0])
        result = ''
        parent = aliased(Page)
        query = Page.query.join(parent, "parent").filter(parent.slug == slug)
        for page in query:
            path = url_for('pages.view_page', slug=page.slug)
            result += caller(page.menu_title, path, request.path == path)
        return result
