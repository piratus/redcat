import os
import Image

from website.utils import extend_filename


def create_thumbnail(full_path, size='150x150', fit=True):
    """Create thumbnail for a file.
    @param full_path: str - path to file
    @param size: str - size of thumbnail in 'WxH' format
    @param fit: bool - crop the image to fit the box
    """
    box = [int(x) for x in size.split('x')]
    if len(box) == 1:
        box *= 2

    if not len(box) == 2:
        raise ValueError('Incorrect size: {0}'.format(size))

    path, filename = os.path.split(full_path)
    image = Image.open(full_path)
    new_filename = extend_filename(filename, size)
    with file(os.path.join(path, new_filename), 'w') as out:
        resize(image, box, fit, out)


def resize(img, box, fit, out, quality=75):
    """Downsample the image.
    @param img: Image -  an Image-object
    @param box: tuple(x, y) - the bounding box of the result image
    @param fit: boolean - crop the image to fill the box
    @param out: file-like-object - save the image into the output stream
    """
    #preresize image with factor 2, 4, 8 and fast algorithm
    factor = 1
    while img.size[0] / factor > 2 * box[0] and \
            img.size[1] * 2 / factor > 2 * box[1]:
        factor *= 2
    if factor > 1:
        img.thumbnail((img.size[0] / factor, img.size[1] / factor),
                Image.NEAREST)

    #calculate the cropping box and get the cropped part
    if fit:
        x1 = y1 = 0
        x2, y2 = img.size
        w_ratio = 1.0 * x2 / box[0]
        h_ratio = 1.0 * y2 / box[1]
        if h_ratio > w_ratio:
            y1 = int(y2 / 2 - box[1] * w_ratio / 2)
            y2 = int(y2 / 2 + box[1] * w_ratio / 2)
        else:
            x1 = int(x2 / 2 - box[0] * h_ratio / 2)
            x2 = int(x2 / 2 + box[0] * h_ratio / 2)
        img = img.crop((x1, y1, x2, y2))

    #Resize the image with best quality algorithm ANTI-ALIAS
    img.thumbnail(box, Image.ANTIALIAS)

    #save it into a file-like object
    img.save(out, "JPEG", quality=quality)
