# coding: utf-8
"""redcatstudio.com.ua website
@author: Andrew Popovych <piratus@gmail.com>
"""
import os
from flask import Flask, render_template
from website.database import db, Photo
from website.assets import assets

# Config
app = Flask(__name__)
app.config.from_pyfile('settings.py')

# Init database
db.init_app(app)

#Init assets
assets.init_app(app)


# Views
@app.route('/')
def index():
    """Main view where the portfolio will be"""
    return render_template('index.html',
                           selected='index',
                           photos=Photo.query.limit(32).all())


from .views import admin, gallery, pages, panoramas
app.register_blueprint(admin)
app.register_blueprint(gallery)
app.register_blueprint(pages)
app.register_blueprint(panoramas)

from .views.pages import MenuExtension
app.jinja_env.add_extension(MenuExtension)


def static(pagename):
    """Displays template and adds it's name to params for navigation"""
    def view():
        return render_template('{0}.html'.format(pagename), selected=pagename)
    return view

@app.before_first_request
def ensure_upload_paths():
    for path in [app.config['PHOTO_PATH'], app.config['PANORAMA_PATH']]:
        if not os.path.exists(path):
            os.makedirs(path)
