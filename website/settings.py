"""Settings for redcat"""
import os
_basepath = os.path.dirname(__file__)

DEBUG = True

SQLALCHEMY_DATABASE_URI = 'postgres://piratus:@localhost/redcat'
SQLALCHEMY_ECHO = DEBUG

SECRET_KEY = 'development secret'

ADMINS = (
    ('admin', 'password', ),
)

PHOTO_PATH = os.path.join(_basepath, 'static', 'photos')
PANORAMA_PATH = os.path.join(_basepath, 'static', 'panoramas')

MAX_CONTENT_LENGTH = 60 * 1024 * 1024

try:
    from settings_local import *
except ImportError:
    pass
