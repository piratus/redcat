# coding: utf-8
""" Utility functions for redcat project """
from itertools import chain


def extend_filename(filename, extra, *args):
    """Add data to the end of filename with respect
    to extension.

    >>> extend_filename('test.png', 'extra')
    'testextra.png'
    >>> extend_filename('test.png', 150, 150)
    'test150_150.png'
    >>> extend_filename('test', 'noextension')
    'testnoextension'
    >>>
    """
    extra = '_'.join(str(char) for char in chain([extra], args))

    if '.' in filename:
        name, ext = filename.split('.')
        return '{0}{1}.{2}'.format(name, extra, ext)
    else:
        name, ext = filename, ''
        return '{0}{1}'.format(name, extra)
