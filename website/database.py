# coding: utf-8
"""Database operations and models"""
from datetime import datetime
import os
from flask import url_for
from flask.ext.sqlalchemy import SQLAlchemy
from pytils.translit import slugify
from website.settings import PANORAMA_PATH


db = SQLAlchemy()


class JSONSerializable(object):
    """Mixin for serializing a model to json"""
    def to_json(self):
        """Return a dict of columns and values"""
        columns = self.__table__.columns.keys()
        return {key: getattr(self, key) for key in columns}


class Album(db.Model, JSONSerializable):
    """Represents an album"""
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(50), unique=True)
    slug = db.Column(db.String(50), unique=True)
    order = db.Column(db.Integer, default=0)

    photos = db.relationship('Photo', backref='album', lazy='joined',
                             cascade='all, delete', order_by='Photo.order')

    def __init__(self, name, slug=None, order=0):
        self.name = name
        self.slug = slug or slugify(self.name)
        self.order = order

    @property
    def cover(self):
        try:
            photo = self.photos[0]
        except IndexError:
            return ''
        return '{0}?size=435x200'.format(photo.url)

    def __repr__(self):
        return u'<Album: {0}>'.format(self.name)


class Photo(db.Model):
    """Represents a photo in an album"""
    id = db.Column(db.Integer, primary_key=True)
    album_id = db.Column(db.Integer, db.ForeignKey('album.id'))

    path = db.Column(db.String(255))
    order = db.Column(db.Integer, default=0)
    date_added = db.Column(db.DateTime)

    def __init__(self, album, path, order=0):
        self.album = album
        self.path = path
        self.order = order
        self.date_added = datetime.now()

    def __repr__(self):
        return u'<Photo: {}>'.format(self.path)

    @property
    def url(self):
        return url_for('gallery.show_photo', filename=self.path)

    def to_json(self):
        return dict(
            id = self.id,
            album_id = self.album_id,
            url = self.url,
            order = self.order
        )


class Panorama(db.Model):
    FILE_NAMES = {
        'image': ('preview.jpg', 'preview_image'),
        'hq': ('pano4000.swf', 'hq_file_size'),
        'mq': ('pano8000.swf', 'mq_file_size'),
    }

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    description = db.Column(db.Text, nullable=True, default=None)
    date_added = db.Column(db.DateTime)
    published = db.Column(db.Boolean, default=False)

    has_preview = db.Column(db.Boolean, default=False)
    hq_file_size = db.Column(db.Integer, nullable=True, default=None)
    mq_file_size = db.Column(db.Integer, nullable=True, default=None)

    def __init__(self, name, description=None):
        self.name = name
        self.description = description
        self.date_added = datetime.now()

    def __repr__(self):
        return u'<Panorama: {}>'.format(self.id)

    @property
    def preview_image(self):
        filename = 'panoramas/{}/preview435x200.jpg'.format(self.id)
        return url_for('static', filename=filename)

    @property
    def upload_path(self):
        return os.path.join(PANORAMA_PATH, str(self.id))

    def url_for(self, quality):
        return url_for('panoramas.view_panorama', pano_id=self.id) + '?quality=' + quality

    def file_for(self, quality):
        base = 'panoramas/{}/'.format(self.id)
        return base + Panorama.FILE_NAMES[quality][0]

    def to_json(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description or '',
            'preview_image': self.preview_image if self.has_preview else None,
            'published': self.published,
            'hq_file_size': self.hq_file_size,
            'mq_file_size': self.mq_file_size
        }


PAGES_TEMPLATES = {
    'basic': 'pages/basic.html',
    'contacts': 'pages/contacts.html'
}


class Page(db.Model, JSONSerializable):
    """Represents a page object in the database"""
    id = db.Column(db.Integer, primary_key=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('page.id'), default=None)
    parent = db.relationship('Page', remote_side=[id])
    template = db.Column(db.String(20), default='basic')

    title = db.Column(db.String(255))
    menu_title = db.Column(db.String(100))
    slug = db.Column(db.String(100))

    content = db.Column(db.Text(), default='')

    description = db.Column(db.String(255), default='')
    keywords = db.Column(db.String(255), default='')

    order = db.Column(db.Integer, default=0)
    published = db.Column(db.Boolean, default=False)

    def __init__(self, title='', menu_title='', slug='', content='',
                 parent=None, description='', keywords='',
                 order=0, published=False):
        self.title = title
        self.parent = parent
        self.menu_title = menu_title
        self.slug = slug
        self.content = content
        self.description = description
        self.keywords = keywords
        self.order = order
        self.published = published

    def __repr__(self):
        return u'<Page: {0}>'.format(self.title)


class Message(db.Model, JSONSerializable):
    """Represents messages from contacts page."""
    id = db.Column(db.Integer, primary_key=True)
    new = db.Column(db.Boolean, default=True)

    name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    message = db.Column(db.Text)

    def __init__(self, name, email, message):
        self.name = name
        self.email = email
        self.message = message

    def __repr__(self):
        return u'<Message: "{0}"({1})>'.format(self.name, self.email)
