var AlbumEditor, AlbumEditorItem, AlbumsSection, CollectionsManager, Dialog, FileField, Form, FormDialog, ImageField, MARKITUP_BUTTONS, PageEditor, PagesMenu, PagesMenuItem, PanoramaEditor, PanoramasSection, Router, Sidebar, SidebarSection, SidebarSectionItem, TextArea, TextField, createAlbumDialog, createPageDialog, createPanoramaDialog, namespace, newID, reorder, reorderPages, _ref, _ref1, _ref10, _ref11, _ref12, _ref13, _ref14, _ref15, _ref16, _ref17, _ref18, _ref19, _ref2, _ref20, _ref21, _ref22, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9,
  _this = this,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

jQuery.fn.serializeObject = function() {
  var arrayData, objectData;

  arrayData = this.serializeArray();
  objectData = {};
  $.each(arrayData, function() {
    var value;

    if (this.value != null) {
      value = this.value;
    } else {
      value = '';
    }
    if (objectData[this.name] != null) {
      if (!objectData[this.name].push) {
        objectData[this.name] = [objectData[this.name]];
      }
      return objectData[this.name].push(value);
    } else {
      return objectData[this.name] = value;
    }
  });
  return objectData;
};

MARKITUP_BUTTONS = [
  {
    name: 'Bold',
    key: 'B',
    openWith: '(!(<strong>|!|<b>)!)',
    closeWith: '(!(</strong>|!|</b>)!)'
  }, {
    name: 'Italic',
    key: 'I',
    openWith: '(!(<em>|!|<i>)!)',
    closeWith: '(!(</em>|!|</i>)!)'
  }, {
    name: 'Stroke through',
    key: 'S',
    openWith: '<del>',
    closeWith: '</del>'
  }, {
    separator: '---------------'
  }, {
    name: 'Bulleted List',
    openWith: '    <li>',
    closeWith: '</li>',
    multiline: true,
    openBlockWith: '<ul>\n',
    closeBlockWith: '\n</ul>'
  }, {
    name: 'Numeric List',
    openWith: '    <li>',
    closeWith: '</li>',
    multiline: true,
    openBlockWith: '<ol>\n',
    closeBlockWith: '\n</ol>'
  }, {
    separator: '---------------'
  }, {
    name: 'Picture',
    key: 'P',
    replaceWith: '<img src="[![Source:!:http://]!]" alt="[![Alternative text]!]" />'
  }, {
    name: 'Link',
    key: 'L',
    openWith: '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>',
    closeWith: '</a>',
    placeHolder: 'Your text to link...'
  }, {
    separator: '---------------'
  }, {
    name: 'Clean',
    className: 'clean',
    replaceWith: function(markitup) {
      return markitup.selection.replace(/<(.*?)>/g, '');
    }
  }, {
    name: 'Preview',
    className: 'preview',
    call: 'preview'
  }
];

this.MARKITUP_SETTINGS = {
  onShiftEnter: {
    keepDefault: false,
    replaceWith: '<br />\n'
  },
  onCtrlEnter: {
    keepDefault: false,
    openWith: '\n<p>',
    closeWith: '</p>'
  },
  onTab: {
    keepDefault: false,
    replaceWith: '    '
  },
  markupSet: MARKITUP_BUTTONS,
  previewInWindow: 'width=800, height=600, resizable=yes, scrollbars=yes'
};

namespace = function(path) {
  var name, root, _i, _len, _ref, _ref1;

  root = _this;
  _ref = path.split();
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    name = _ref[_i];
    if ((_ref1 = root[name]) == null) {
      root[name] = {};
    }
    root = root[name];
  }
  return root;
};

reorder = function(elements, collection) {
  var index, item, model, order, result, _i, _len;

  result = {};
  for (index = _i = 0, _len = elements.length; _i < _len; index = ++_i) {
    item = elements[index];
    order = index + 1;
    model = collection.get($(item).data('id'));
    if (model.get('order') !== index + 1) {
      model.set({
        order: order
      }, {
        silent: true
      });
      result[model.id] = index + 1;
    }
  }
  return result;
};

newID = (function() {
  var closure, counter;

  counter = 1;
  return closure = function() {
    return "rc-" + (counter++);
  };
})();

Form = (function() {
  Form.prototype.defaults = {
    fields: []
  };

  Form.prototype.fieldDefaults = {
    type: 'text',
    cls: 'span5'
  };

  function Form(options) {
    this.setValues = __bind(this.setValues, this);
    this.values = __bind(this.values, this);
    this.render = __bind(this.render, this);
    var field;

    this.options = _.extend({}, this.defaults, options);
    this.el = document.createElement('form');
    this.$el = $(this.el);
    this.fields = (function() {
      var _i, _len, _ref, _results;

      _ref = this.options.fields;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        field = _ref[_i];
        _results.push(this._createField(_.extend({}, this.fieldDefaults, field)));
      }
      return _results;
    }).call(this);
  }

  Form.prototype.render = function() {
    var field, _i, _len, _ref;

    _ref = this.fields;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      field = _ref[_i];
      this.$el.append(field.render().el);
    }
    if (this.options.values) {
      this.setValues(this.options.values);
    }
    return this;
  };

  Form.prototype.values = function() {
    var field, values, _i, _len, _ref;

    values = {};
    _ref = this.fields;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      field = _ref[_i];
      values[field.name] = field.getValue();
    }
    return values;
  };

  Form.prototype.setValues = function(values) {
    var field, value, _i, _len, _ref;

    _ref = this.fields;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      field = _ref[_i];
      value = values[field.name];
      if (value != null) {
        field.setValue(value);
      }
    }
    return this;
  };

  Form.prototype._createField = function(config) {
    config = _.extend({}, this.fieldDefaults, config);
    switch (config.type) {
      case 'text':
        return new TextField(config);
      case 'textarea':
        return new TextArea(config);
    }
  };

  return Form;

})();

TextField = (function() {
  TextField.prototype.template = _.template("<label for=\"<%= id %>\"><%= label %></label>\n<input id=\"<%= id %>\" class=\"<%= cls %>\" name=\"<%= name %>\" type=\"<%= type %>\" value=\"<%= value %>\">");

  TextField.prototype.defaults = {
    cls: '',
    value: ''
  };

  function TextField(options) {
    this.setValue = __bind(this.setValue, this);
    this.getValue = __bind(this.getValue, this);
    this.render = __bind(this.render, this);
    var _base, _base1, _base2, _ref, _ref1, _ref2;

    this.options = _.extend({}, this.defaults, options);
    this.el = document.createElement('div');
    this.id = (_ref = (_base = this.options).id) != null ? _ref : _base.id = newID();
    this.name = (_ref1 = (_base1 = this.options).name) != null ? _ref1 : _base1.name = this.options.id;
    if ((_ref2 = (_base2 = this.options).label) == null) {
      _base2.label = this.options.name;
    }
  }

  TextField.prototype.render = function() {
    $(this.el).addClass('clearfix').html(this.template(this.options));
    return this;
  };

  TextField.prototype.getValue = function() {
    return $('#' + this.options.id, this.el).val();
  };

  TextField.prototype.setValue = function(value) {
    return $('#' + this.options.id, this.el).val(value);
  };

  return TextField;

})();

TextArea = (function(_super) {
  __extends(TextArea, _super);

  function TextArea() {
    _ref = TextArea.__super__.constructor.apply(this, arguments);
    return _ref;
  }

  TextArea.prototype.template = _.template("<label for=\"<%= id %>\"><%= label %></label>\n<textarea id=\"<%= id %>\" class=\"<%= cls %>\" name=\"<%= name %>\"><%= value %></textarea>");

  return TextArea;

})(TextField);

Dialog = (function() {
  Dialog.prototype.template = _.template("<div class=\"modal-header\"><a href=\"#\" class=\"close\" data-dismiss=\"modal\">×</a><h3><%= title %></h3></div>\n<div class=\"modal-body\"><%= content %></div>\n<div class=\"modal-footer\"></div>");

  Dialog.prototype.defaults = {
    title: '',
    content: '',
    show: true,
    keyboard: true,
    backdrop: true,
    buttons: [],
    cls: ''
  };

  function Dialog(options) {
    this._click = __bind(this._click, this);
    this.hide = __bind(this.hide, this);
    this.show = __bind(this.show, this);
    var button, handler, self, _i, _len, _ref1;

    this.options = _.extend({}, this.defaults, options);
    $(this.el = document.createElement('div')).addClass("modal hide " + this.options.cls).html(this.template(this.options)).modal({
      show: this.options.show,
      keyboard: this.options.keyboard,
      backdrop: this.options.backdrop
    });
    this.titleEl = $('.modal-header h3', this.el);
    this.contentEl = $('.modal-body', this.el);
    this.footerEl = $('.modal-footer', this.el);
    self = this;
    handler = function() {
      var action;

      action = $(this).data('action');
      return self._click(action);
    };
    _ref1 = this.options.buttons;
    for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
      button = _ref1[_i];
      $(this._createButton(button)).click(handler).appendTo(this.footerEl);
    }
    if (this.options.buttons.length === 0) {
      this.footerEl.hide();
    }
  }

  Dialog.prototype.show = function() {
    return $(this.el).modal('show');
  };

  Dialog.prototype.hide = function() {
    return $(this.el).modal('hide');
  };

  Dialog.prototype._click = function(type) {
    if (this.options.callback) {
      return this.options.callback.call(this, type);
    }
  };

  Dialog.prototype._createButton = function(type) {
    switch (type) {
      case 'save':
        return '<span class="btn btn-primary" data-action="save">Сохранить</span>';
      case 'delete':
        return '<span class="btn btn-danger pull-left" data-action="delete">Удалить</span>';
    }
  };

  return Dialog;

})();

FormDialog = (function(_super) {
  __extends(FormDialog, _super);

  function FormDialog(options) {
    this._click = __bind(this._click, this);    options.content = '';
    options.cls += ' form-dialog';
    FormDialog.__super__.constructor.call(this, options);
    this.form = new Form(this.options.form);
    this.contentEl.html(this.form.render().el);
  }

  FormDialog.prototype._click = function(type) {
    if (this.options.callback) {
      return this.options.callback.call(this, type, this.form.values());
    }
  };

  return FormDialog;

})(Dialog);

if ((_ref1 = this.models) == null) {
  this.models = {};
}

this.models.Album = (function(_super) {
  __extends(Album, _super);

  function Album() {
    this.appPath = __bind(this.appPath, this);
    this.url = __bind(this.url, this);    _ref2 = Album.__super__.constructor.apply(this, arguments);
    return _ref2;
  }

  Album.prototype.url = function() {
    var id;

    id = this.get('id');
    if (id) {
      return "/admin/api/albums/" + id + "/";
    } else {
      return "/admin/api/albums/";
    }
  };

  Album.prototype.appPath = function() {
    return "/albums/" + this.id;
  };

  Album.prototype.parse = function(response) {
    return response.album || response;
  };

  return Album;

})(Backbone.Model);

this.models.AlbumList = (function(_super) {
  __extends(AlbumList, _super);

  function AlbumList() {
    _ref3 = AlbumList.__super__.constructor.apply(this, arguments);
    return _ref3;
  }

  AlbumList.prototype.model = models.Album;

  AlbumList.prototype.url = function() {
    return '/admin/api/albums/';
  };

  AlbumList.prototype.comparator = function(album) {
    return album.get('order');
  };

  AlbumList.prototype.parse = function(response) {
    return response.albums;
  };

  return AlbumList;

})(Backbone.Collection);

this.models.Photo = (function(_super) {
  __extends(Photo, _super);

  function Photo() {
    _ref4 = Photo.__super__.constructor.apply(this, arguments);
    return _ref4;
  }

  Photo.prototype.url = function() {
    return "/admin/api/photos/" + (this.get('id')) + "/";
  };

  Photo.prototype.thumb = function() {
    return "" + (this.get('url')) + "?size=100";
  };

  return Photo;

})(Backbone.Model);

this.models.PhotoList = (function(_super) {
  __extends(PhotoList, _super);

  PhotoList.prototype.model = models.Photo;

  function PhotoList(albumId) {
    this.albumId = albumId;
    if (this.albumId == null) {
      throw "Album ID is not specified";
    }
    PhotoList.__super__.constructor.apply(this, arguments);
  }

  PhotoList.prototype.url = function() {
    return "/admin/api/albums/" + this.albumId + "/";
  };

  PhotoList.prototype.comparator = function(photo) {
    return photo.get('order');
  };

  PhotoList.prototype.parse = function(response) {
    return response.photos;
  };

  return PhotoList;

})(Backbone.Collection);

this.models.Panorama = (function(_super) {
  __extends(Panorama, _super);

  function Panorama() {
    this.appPath = __bind(this.appPath, this);
    this.url = __bind(this.url, this);    _ref5 = Panorama.__super__.constructor.apply(this, arguments);
    return _ref5;
  }

  Panorama.prototype.url = function() {
    var id;

    id = this.get('id');
    if (id) {
      return "/admin/api/panoramas/" + id + "/";
    } else {
      return "/admin/api/panoramas/";
    }
  };

  Panorama.prototype.appPath = function() {
    return "/panoramas/" + this.id;
  };

  Panorama.prototype.parse = function(response) {
    return response.panorama || response;
  };

  Panorama.prototype.togglePublished = function(options) {
    var data;

    if (options == null) {
      options = {};
    }
    data = {
      published: !this.get('published')
    };
    return this.save(data, options);
  };

  return Panorama;

})(Backbone.Model);

this.models.PanoramaList = (function(_super) {
  __extends(PanoramaList, _super);

  function PanoramaList() {
    _ref6 = PanoramaList.__super__.constructor.apply(this, arguments);
    return _ref6;
  }

  PanoramaList.prototype.model = models.Panorama;

  PanoramaList.prototype.url = function() {
    return '/admin/api/panoramas/';
  };

  PanoramaList.prototype.parse = function(response) {
    return response.panoramas;
  };

  return PanoramaList;

})(Backbone.Collection);

this.models.Page = (function(_super) {
  __extends(Page, _super);

  function Page() {
    this.appPath = __bind(this.appPath, this);    _ref7 = Page.__super__.constructor.apply(this, arguments);
    return _ref7;
  }

  Page.prototype.defaults = {
    title: '',
    menu_title: '',
    slug: '',
    content: '',
    description: '',
    keywords: '',
    published: false
  };

  Page.prototype.url = function() {
    var id;

    if (id = this.get('id')) {
      return "/admin/api/pages/" + id + "/";
    } else {
      return '/admin/api/pages/';
    }
  };

  Page.prototype.appPath = function() {
    return "/pages/" + this.id;
  };

  Page.prototype.parse = function(response) {
    return response.page || response;
  };

  Page.prototype.togglePublished = function(options) {
    var data;

    if (options == null) {
      options = {};
    }
    data = {
      published: !this.get('published')
    };
    return this.save(data, options);
  };

  return Page;

})(Backbone.Model);

this.models.PageList = (function(_super) {
  __extends(PageList, _super);

  function PageList() {
    _ref8 = PageList.__super__.constructor.apply(this, arguments);
    return _ref8;
  }

  PageList.prototype.model = models.Page;

  PageList.prototype.url = function() {
    return '/admin/api/pages/';
  };

  PageList.prototype.parse = function(response) {
    return response.pages;
  };

  PageList.prototype.toTree = function() {
    var rootItem, rootItems, tree, _i, _len;

    rootItems = this.filter(function(item) {
      return item.get('parent_id') === null;
    });
    tree = new Backbone.Collection(rootItems);
    for (_i = 0, _len = rootItems.length; _i < _len; _i++) {
      rootItem = rootItems[_i];
      rootItem.children = this.filter(function(item) {
        return item.get('parent_id') === rootItem.id;
      });
    }
    return tree;
  };

  return PageList;

})(Backbone.Collection);

createAlbumDialog = function(type, _arg) {
  var callback, config, values;

  values = _arg.values, callback = _arg.callback;
  config = {
    title: 'Новый альбом',
    buttons: ['save'],
    form: {
      fields: [
        {
          name: 'name',
          label: 'Название'
        }, {
          name: 'slug',
          label: 'Путь в URL'
        }
      ],
      values: values || {}
    }
  };
  if (type === 'edit') {
    _.extend(config, {
      title: "Альбом «" + values.name + "»",
      buttons: ['delete', 'save']
    });
  }
  if (callback != null) {
    _.extend(config, {
      callback: callback
    });
  }
  return new FormDialog(config);
};

AlbumEditorItem = (function(_super) {
  __extends(AlbumEditorItem, _super);

  function AlbumEditorItem() {
    this.remove = __bind(this.remove, this);
    this.render = __bind(this.render, this);    _ref9 = AlbumEditorItem.__super__.constructor.apply(this, arguments);
    return _ref9;
  }

  AlbumEditorItem.prototype.tagName = 'li';

  AlbumEditorItem.prototype.template = _.template("<a data-id=\"<%= id %>\" class=\"thumbnail\">\n  <img height=\"100\" width=\"100\" src=\"<%= url %>?size=100\">\n  <i class=\"icon-remove\"></i>\n</a>");

  AlbumEditorItem.prototype.events = {
    'click .icon-remove': 'remove'
  };

  AlbumEditorItem.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  };

  AlbumEditorItem.prototype.remove = function() {
    return this.model.destroy();
  };

  return AlbumEditorItem;

})(Backbone.View);

AlbumEditor = (function(_super) {
  __extends(AlbumEditor, _super);

  function AlbumEditor() {
    this.destroy = __bind(this.destroy, this);
    this.uploadFinished = __bind(this.uploadFinished, this);
    this.progressHandler = __bind(this.progressHandler, this);
    this.updateOrder = __bind(this.updateOrder, this);
    this.updateName = __bind(this.updateName, this);
    this.renderItem = __bind(this.renderItem, this);    _ref10 = AlbumEditor.__super__.constructor.apply(this, arguments);
    return _ref10;
  }

  AlbumEditor.prototype.template = _.template("<h3 class=\"page-title\"><%= name %></h3>\n<div class=\"actions\">\n  <span class=\"btn edit\">Параметры</span>\n</div>\n<div class=\"progress\"  style=\"display: none;\"><span class=\"bar\"></span></div>\n<div class=\"dropzone centered\">\n  <i class=\"icon-download-alt\"></i> файлы бросать сюда\n  <input type=\"file\" class=\"fileinput\">\n</div>\n<ul class=\"thumbnails\"></ul>");

  AlbumEditor.prototype.events = {
    'click .edit': 'edit'
  };

  AlbumEditor.prototype.className = 'hero-unit';

  AlbumEditor.prototype.itemClass = AlbumEditorItem;

  AlbumEditor.prototype.initialize = function() {
    this.albumId = this.model.id;
    this.model.on('change:name', this.updateName);
    this.model.on('destroy', this.destroy);
    this.collection = new models.PhotoList(this.albumId);
    this.collection.on('reset', this.render);
    return this.collection.on('add', this.renderItem);
  };

  AlbumEditor.prototype.render = function() {
    var _this = this;

    this.$el.html(this.template(this.model.toJSON())).addClass(this.className);
    this.$('.thumbnails').sortable({
      stop: this.updateOrder,
      handle: 'img'
    });
    $('body').on('keydown', function(event) {
      if (event.altKey) {
        return _this.$el.addClass('deleteMode');
      }
    });
    $('body').on('keyup', function(event) {
      if (!event.altKey) {
        return _this.$el.removeClass('deleteMode');
      }
    });
    this.initFileUpload();
    this.collection.fetch();
    return this;
  };

  AlbumEditor.prototype.renderItem = function(model) {
    var view;

    view = new this.itemClass({
      model: model,
      parent: this
    });
    return this.$('.thumbnails').append(view.render().el);
  };

  AlbumEditor.prototype.initFileUpload = function() {
    return this.$('input[type=file]').fileupload({
      url: '/admin/api/photos/',
      dataType: 'json',
      paramName: 'photo',
      formData: {
        album_id: this.albumId
      },
      dropZone: this.$el,
      sequentialUploads: true,
      limitConcurrentUploads: 1,
      progressall: this.progressHandler,
      done: this.uploadFinished
    });
  };

  AlbumEditor.prototype.updateName = function() {
    return this.$('.page-title').html(this.model.get('name'));
  };

  AlbumEditor.prototype.updateOrder = function() {
    return $.post("/admin/api/albums/" + this.albumId + "/update_order/", reorder(this.$('ul a'), this.collection));
  };

  AlbumEditor.prototype.progressHandler = function(e, data) {
    var progress;

    this.$('.progress').show();
    progress = parseInt(data.loaded / data.total * 100, 10);
    return this.$('.bar').css('width', progress + '%');
  };

  AlbumEditor.prototype.uploadFinished = function(e, data) {
    var message, photo, success, _ref11;

    this.$('.progress').hide();
    _ref11 = data.result, success = _ref11.success, photo = _ref11.photo, message = _ref11.message;
    if (success === true) {
      return this.collection.add([photo]);
    } else {
      return alert(message || 'Что-то пошло не так');
    }
  };

  AlbumEditor.prototype.edit = function() {
    var dialog,
      _this = this;

    return dialog = createAlbumDialog('edit', {
      values: this.model.toJSON(),
      callback: function(type, data) {
        switch (type) {
          case 'save':
            return _this.model.save(data, {
              success: function() {
                return dialog.hide().remove();
              }
            });
          case 'delete':
            if (confirm('Удалить альбом?')) {
              return _this.model.destroy({
                success: function() {
                  return dialog.hide().remove();
                }
              });
            }
        }
      }
    });
  };

  AlbumEditor.prototype.destroy = function() {
    this.$el.html('');
    return typeof router !== "undefined" && router !== null ? router.navigate('', {
      trigger: true
    }) : void 0;
  };

  return AlbumEditor;

})(Backbone.View);

createPanoramaDialog = function(type, _arg) {
  var callback, config, values;

  values = _arg.values, callback = _arg.callback;
  config = {
    title: 'Новая панорама',
    buttons: ['save'],
    form: {
      fields: [
        {
          name: 'name',
          label: 'Название'
        }
      ],
      values: values || {}
    }
  };
  if (type === 'edit') {
    _.extend(config, {
      title: "Панорама «" + values.name + "»",
      buttons: ['delete', 'save']
    });
  }
  if (callback != null) {
    _.extend(config, {
      callback: callback
    });
  }
  return new FormDialog(config);
};

FileField = (function(_super) {
  __extends(FileField, _super);

  function FileField() {
    this.progressHandler = __bind(this.progressHandler, this);
    this.uploadFinished = __bind(this.uploadFinished, this);    _ref11 = FileField.__super__.constructor.apply(this, arguments);
    return _ref11;
  }

  FileField.prototype.template = _.template("<label class=\"control-label\"><%= label %></label>\n<div class=\"controls\">\n  <input type=\"file\" class=\"fileinput\" accept=\"<%= accept %>\"/>\n  <div class=\"display <%= displayClass %>\"></div>\n  <a class=\"btn btn-primary upload\" href=\"#\">Загрузить</a>\n</div>");

  FileField.prototype.className = 'control-group';

  FileField.prototype.label = 'field';

  FileField.prototype.displayClass = '';

  FileField.prototype.accept = 'application/x-shockwave-flash';

  FileField.prototype.successTemplate = _.template('<i class="icon icon-check"></i> <span><%= value %></span>');

  FileField.prototype.errorTemplate = _.template('<i class="icon icon-remove"></i> <span><%= message %></span>');

  FileField.prototype.events = {
    'click .upload': 'selectUpload'
  };

  FileField.prototype.initialize = function(options) {
    if (options.label) {
      this.label = options.label;
    }
    if (options.displayClass) {
      this.displayClass = options.displayClass;
    }
    if (options.type) {
      this.fileType = options.type;
    }
    if (options.field) {
      return this.fieldName = options.field;
    }
  };

  FileField.prototype.render = function() {
    var displayedValue, value;

    this.$el.html(this.template(this));
    value = this.model.get(this.fieldName);
    displayedValue = value ? this.successTemplate({
      value: value
    }) : this.errorTemplate({
      message: 'файл не загружен'
    });
    this.display = this.$el.find('.display').html(displayedValue);
    this.$('input[type=file]').fileupload({
      url: this.model.url(),
      dataType: 'json',
      paramName: this.fileType,
      progressall: this.progressHandler,
      done: this.uploadFinished,
      type: 'PUT'
    });
    return this;
  };

  FileField.prototype.uploadFinished = function(e, data) {
    var message, panorama, success, _ref12;

    _ref12 = data.result, success = _ref12.success, panorama = _ref12.panorama, message = _ref12.message;
    if (success === true) {
      this.model.set(panorama);
      return this.display.html(this.successTemplate({
        value: panorama[this.fieldName]
      }));
    } else {
      this.model.set(this.fieldName, false);
      return this.display.html(this.errorTemplate({
        message: message != null ? message : 'файл не загружен'
      }));
    }
  };

  FileField.prototype.selectUpload = function(event) {
    this.$('input[type=file]').trigger('click');
    return event.preventDefault();
  };

  FileField.prototype.progressHandler = function(e, data) {
    var progress;

    if (!this.$('.progress').length) {
      this.display.html('<div class="progress" style="width: 100%;"><div class="bar" style="width: 0;"></div></div>');
    }
    progress = parseInt(data.loaded / data.total * 100, 10);
    return this.$('.bar').css('width', progress + '%');
  };

  return FileField;

})(Backbone.View);

ImageField = (function(_super) {
  __extends(ImageField, _super);

  function ImageField() {
    _ref12 = ImageField.__super__.constructor.apply(this, arguments);
    return _ref12;
  }

  ImageField.prototype.label = 'Картинка';

  ImageField.prototype.displayClass = 'preview';

  ImageField.prototype.fileType = 'image';

  ImageField.prototype.fieldName = 'preview_image';

  ImageField.prototype.accept = 'image/jpeg';

  ImageField.prototype.successTemplate = _.template('<img src="<%= value %>?<%= (new Date()).getTime() %>">');

  return ImageField;

})(FileField);

PanoramaEditor = (function(_super) {
  __extends(PanoramaEditor, _super);

  function PanoramaEditor() {
    this.destroy = __bind(this.destroy, this);    _ref13 = PanoramaEditor.__super__.constructor.apply(this, arguments);
    return _ref13;
  }

  PanoramaEditor.prototype.template = _.template("<h3 class=\"page-title\"><%= name %></h3>\n<div class=\"actions\">\n  <div class=\"btn-group\">\n    <span class=\"btn edit\">Параметры</span>\n  </div>\n  <div class=\"btn-group\">\n    <span class=\"btn publish\">Опубликовать</span>\n  </div>\n</div>\n<form class=\"form-horizontal panorama-form\"></form>");

  PanoramaEditor.prototype.className = 'hero-unit';

  PanoramaEditor.prototype.events = {
    'click .preview': 'togglePreview',
    'click .edit': 'edit',
    'click .publish': 'publish'
  };

  PanoramaEditor.prototype.initialize = function() {
    this.model.bind('change:name', this.updateName, this);
    this.model.bind('change:published', this.updatePublished, this);
    return this.model.bind('destroy', this.destroy);
  };

  PanoramaEditor.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON())).addClass(this.className);
    this.$el.find('form').append((new ImageField({
      model: this.model
    })).render().el).append((new FileField({
      model: this.model,
      label: 'Высокое качество',
      type: 'hq',
      field: 'hq_file_size'
    })).render().el).append((new FileField({
      model: this.model,
      label: 'Среднее качество',
      type: 'mq',
      field: 'mq_file_size'
    })).render().el);
    this.$publishBtn = this.$('.publish').tooltip();
    this.updatePublished();
    return this;
  };

  PanoramaEditor.prototype.togglePreview = function(event) {
    return $(event.currentTarget).toggleClass('expanded');
  };

  PanoramaEditor.prototype.publish = function() {
    return this.model.togglePublished();
  };

  PanoramaEditor.prototype.updatePublished = function() {
    var published;

    published = this.model.get('published');
    this.$publishBtn.removeClass('btn-success btn-danger').text(published ? 'Опубликована' : 'Скрыта').addClass(published ? 'btn-success' : 'btn-danger');
    return this;
  };

  PanoramaEditor.prototype.updateName = function() {
    return this.$('.page-title').html(this.model.get('name'));
  };

  PanoramaEditor.prototype.edit = function() {
    var dialog,
      _this = this;

    return dialog = createPanoramaDialog('edit', {
      values: this.model.toJSON(),
      callback: function(type, data) {
        switch (type) {
          case 'save':
            return _this.model.save(data, {
              success: function() {
                return dialog.hide().remove();
              }
            });
          case 'delete':
            if (_this.model.get('published') === false) {
              return _this.model.destroy({
                success: function() {
                  return dialog.hide().remove();
                }
              });
            }
        }
      }
    });
  };

  PanoramaEditor.prototype.destroy = function() {
    this.$el.html('');
    return typeof router !== "undefined" && router !== null ? router.navigate('', {
      trigger: true
    }) : void 0;
  };

  return PanoramaEditor;

})(Backbone.View);

createPageDialog = function(type, _arg) {
  var callback, config, values;

  values = _arg.values, callback = _arg.callback;
  config = {
    title: 'Новая страница',
    buttons: ['save'],
    form: {
      fields: [
        {
          name: 'title',
          label: 'Заголовок'
        }, {
          name: 'menu_title',
          label: 'Заголовок в меню'
        }, {
          name: 'slug',
          label: 'Путь в URL'
        }
      ],
      values: values
    },
    callback: callback
  };
  if (type === 'edit') {
    _.extend(config, {
      title: "Cтраница «" + values.menu_title + "»",
      buttons: ['delete', 'save']
    });
    config.form.fields = _.union(config.form.fields, [
      {
        name: 'description',
        type: 'textarea',
        label: 'Description'
      }, {
        name: 'keywords',
        type: 'textarea',
        label: 'Keywords'
      }
    ]);
  }
  return new FormDialog(config);
};

reorderPages = function(items, collection) {
  var data, item, model, order, result, subItem, subOrder, _i, _j, _len, _len1, _ref14;

  result = [];
  for (order = _i = 0, _len = items.length; _i < _len; order = ++_i) {
    item = items[order];
    data = {
      order: order,
      parent_id: null
    };
    model = collection.get(item.id);
    if (model.get('parent_id') !== null || model.get('order') !== order) {
      model.set(data, {
        silent: true
      });
      result.push(_.extend(data, {
        id: item.id
      }));
    }
    _ref14 = item.children || [];
    for (subOrder = _j = 0, _len1 = _ref14.length; _j < _len1; subOrder = ++_j) {
      subItem = _ref14[subOrder];
      data = {
        order: subOrder,
        parent_id: item.id
      };
      model = collection.get(subItem.id);
      if (model.get('parent_id') !== item.id || model.get('order') !== subOrder) {
        model.set(data, {
          silent: true
        });
        result.push(_.extend(data, {
          id: subItem.id
        }));
      }
    }
  }
  return result;
};

PageEditor = (function(_super) {
  __extends(PageEditor, _super);

  function PageEditor() {
    this.save = __bind(this.save, this);
    this.destroy = __bind(this.destroy, this);
    this.render = __bind(this.render, this);    _ref14 = PageEditor.__super__.constructor.apply(this, arguments);
    return _ref14;
  }

  PageEditor.prototype.template = _.template("<h3 class=\"page-title\">\n    <span><%= menu_title %></span>&nbsp;\n</h3>\n<div class=\"actions\">\n  <div class=\"btn-group\">\n    <span class=\"btn edit\">Параметры</span>\n    <span class=\"btn btn-primary save\">Сохранить</span>\n  </div>\n  <div class=\"btn-group\">\n    <span class=\"btn publish\">Опубликовать</span>\n  </div>\n</div>\n<form><textarea id=\"page-content\" style=\"width: 98%\" rows=\"20\"><%= content %></textarea></form>");

  PageEditor.prototype.className = 'hero-unit';

  PageEditor.prototype.events = {
    'click .edit': 'edit',
    'click .save': 'save',
    'click .publish': 'publish'
  };

  PageEditor.prototype.codeMirrorSettings = {
    mode: 'text/html',
    tabMode: 'indent',
    lineNumbers: true
  };

  PageEditor.prototype.initialize = function() {
    this.model.bind('change:menu_title', this.updateTitle, this);
    this.model.bind('change:published', this.updatePublished, this);
    return this.model.bind('destroy', this.destroy);
  };

  PageEditor.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON())).addClass(this.className);
    this.editor = CodeMirror.fromTextArea(this.$('textarea')[0], this.codeMirrorSettings);
    this.$publishBtn = this.$('.publish');
    this.updatePublished();
    this.$publishBtn.tooltip();
    return this;
  };

  PageEditor.prototype.updateTitle = function() {
    return this.$('.page-title > span').html(this.model.get('title'));
  };

  PageEditor.prototype.updatePublished = function() {
    var published;

    published = this.model.get('published');
    this.$publishBtn.removeClass('btn-success btn-danger').text(published ? 'Опубликована' : 'Скрыта').addClass(published ? 'btn-success' : 'btn-danger');
    return this;
  };

  PageEditor.prototype.destroy = function() {
    return $(this.el).html('');
  };

  PageEditor.prototype.edit = function() {
    var model;

    model = this.model;
    return createPageDialog('edit', {
      values: this.model.toJSON(),
      callback: function(type, data) {
        var _this = this;

        switch (type) {
          case 'save':
            return model.save(data, {
              success: function() {
                return _this.hide().remove();
              }
            });
          case 'delete':
            if (model.get('published') === false) {
              return model.destroy({
                success: function() {
                  return _this.hide().remove();
                }
              });
            } else {
              return alert("Нельзя удалять опубликованное!");
            }
        }
      }
    });
  };

  PageEditor.prototype.save = function() {
    this.model.save({
      content: this.editor.getValue()
    });
    return true;
  };

  PageEditor.prototype.publish = function() {
    return this.model.togglePublished();
  };

  return PageEditor;

})(Backbone.View);

SidebarSectionItem = (function(_super) {
  __extends(SidebarSectionItem, _super);

  function SidebarSectionItem() {
    this.remove = __bind(this.remove, this);
    this.update = __bind(this.update, this);
    this.select = __bind(this.select, this);
    this.render = __bind(this.render, this);    _ref15 = SidebarSectionItem.__super__.constructor.apply(this, arguments);
    return _ref15;
  }

  SidebarSectionItem.prototype.tagName = 'li';

  SidebarSectionItem.prototype.template = _.template('<a data-id="<%= id %>"><%= name %></a>');

  SidebarSectionItem.prototype.events = {
    'click a': 'select'
  };

  SidebarSectionItem.prototype.initialize = function(options) {
    this.parent = options.parent;
    this.model.on('change', this.update);
    return this.model.on('destroy', this.remove);
  };

  SidebarSectionItem.prototype.render = function() {
    this.$el.html(this.template(this.model.toJSON()));
    return this;
  };

  SidebarSectionItem.prototype.select = function() {
    this.parent.trigger('selected', this, this.model);
    return false;
  };

  SidebarSectionItem.prototype.update = function() {
    return this.render();
  };

  SidebarSectionItem.prototype.remove = function() {
    return $(this.el).hide({
      speed: 200
    }).remove();
  };

  return SidebarSectionItem;

})(Backbone.View);

SidebarSection = (function(_super) {
  __extends(SidebarSection, _super);

  function SidebarSection() {
    this.addItem = __bind(this.addItem, this);    _ref16 = SidebarSection.__super__.constructor.apply(this, arguments);
    return _ref16;
  }

  SidebarSection.prototype.className = 'sidebar-section';

  SidebarSection.prototype.title = false;

  SidebarSection.prototype.headerTemplate = _.template("<p class=\"nav-header\">\n<%= title %><i class=\"icon-plus add\"></i></p>");

  SidebarSection.prototype.itemClass = SidebarSectionItem;

  SidebarSection.prototype.events = {
    'click .add': 'onAdd'
  };

  SidebarSection.prototype.initialize = function() {
    this.collection.on('add', this.addItem);
    return this.list = $('<ul class="nav nav-list">');
  };

  SidebarSection.prototype.render = function() {
    this.el.innerHTML = this.title ? this.headerTemplate({
      title: this.title
    }) : '';
    this.list.html('').appendTo(this.el);
    this.collection.each(this.addItem);
    return this;
  };

  SidebarSection.prototype.addItem = function(item) {
    var itemView;

    itemView = new this.itemClass({
      model: item,
      parent: this
    });
    return this.list.append(itemView.render().el);
  };

  SidebarSection.prototype.onAdd = function() {};

  SidebarSection.prototype.selectItem = function(id) {
    return this.list.find("[data-id=" + id + "]").closest('li').addClass('active');
  };

  return SidebarSection;

})(Backbone.View);

AlbumsSection = (function(_super) {
  __extends(AlbumsSection, _super);

  function AlbumsSection() {
    this.onAddResult = __bind(this.onAddResult, this);
    this.onAdd = __bind(this.onAdd, this);
    this.updateOrder = __bind(this.updateOrder, this);    _ref17 = AlbumsSection.__super__.constructor.apply(this, arguments);
    return _ref17;
  }

  AlbumsSection.prototype.title = 'Альбомы';

  AlbumsSection.prototype.render = function() {
    AlbumsSection.__super__.render.apply(this, arguments);
    this.list.sortable({
      handle: 'a',
      stop: this.updateOrder
    });
    return this;
  };

  AlbumsSection.prototype.updateOrder = function() {
    return $.post("/admin/api/albums/update_order/", reorder(this.$('ul a'), this.collection));
  };

  AlbumsSection.prototype.onAdd = function() {
    return this.dialog = createAlbumDialog('new', {
      callback: this.onAddResult
    });
  };

  AlbumsSection.prototype.onAddResult = function(type, data) {
    var model,
      _this = this;

    model = new models.Album();
    return model.save(data, {
      success: function(model, response) {
        var _ref18;

        if ((_ref18 = _this.dialog) != null) {
          _ref18.hide().remove();
        }
        return _this.collection.add(model);
      }
    });
  };

  return AlbumsSection;

})(SidebarSection);

PanoramasSection = (function(_super) {
  __extends(PanoramasSection, _super);

  function PanoramasSection() {
    this.onAddResult = __bind(this.onAddResult, this);    _ref18 = PanoramasSection.__super__.constructor.apply(this, arguments);
    return _ref18;
  }

  PanoramasSection.prototype.title = 'Панорамы';

  PanoramasSection.prototype.onAdd = function() {
    return this.dialog = createPanoramaDialog('new', {
      callback: this.onAddResult
    });
  };

  PanoramasSection.prototype.onAddResult = function(type, data) {
    var model,
      _this = this;

    model = new models.Panorama();
    return model.save(data, {
      success: function(model, response) {
        var _ref19;

        if (response.success === true) {
          _this.collection.add([response.panorama]);
        } else {
          alert(response.message || 'Что-то пошло не так');
        }
        return (_ref19 = _this.dialog) != null ? _ref19.hide().remove() : void 0;
      }
    });
  };

  return PanoramasSection;

})(SidebarSection);

PagesMenuItem = (function(_super) {
  __extends(PagesMenuItem, _super);

  function PagesMenuItem() {
    this.update = __bind(this.update, this);    _ref19 = PagesMenuItem.__super__.constructor.apply(this, arguments);
    return _ref19;
  }

  PagesMenuItem.prototype.template = _.template('<a data-id="<%= id %>"><%= menu_title %></a><ul></ul>');

  PagesMenuItem.prototype.initialize = function(options) {
    PagesMenuItem.__super__.initialize.call(this, options);
    return this.children = options.children || [];
  };

  PagesMenuItem.prototype.render = function() {
    var child, childList, view, _i, _len, _ref20, _ref21;

    PagesMenuItem.__super__.render.apply(this, arguments);
    this.$el.attr('id', "page-" + (this.model.get('id')));
    if (((_ref20 = this.children) != null ? _ref20.length : void 0) > 0) {
      childList = this.$('ul');
      _ref21 = this.children;
      for (_i = 0, _len = _ref21.length; _i < _len; _i++) {
        child = _ref21[_i];
        view = new PagesMenuItem({
          model: child,
          parent: this.parent,
          children: []
        });
        childList.append(view.render().el);
      }
    }
    return this;
  };

  PagesMenuItem.prototype.update = function() {
    this.$('a').first().replaceWith(this.template(this.model.toJSON()));
    return this;
  };

  return PagesMenuItem;

})(SidebarSectionItem);

PagesMenu = (function(_super) {
  __extends(PagesMenu, _super);

  function PagesMenu() {
    this.onAddResult = __bind(this.onAddResult, this);
    this.onAdd = __bind(this.onAdd, this);
    this.updateOrder = __bind(this.updateOrder, this);    _ref20 = PagesMenu.__super__.constructor.apply(this, arguments);
    return _ref20;
  }

  PagesMenu.prototype.title = 'Страницы';

  PagesMenu.prototype.itemClass = PagesMenuItem;

  PagesMenu.prototype.render = function() {
    this.list.collection = this.collection.toTree();
    PagesMenu.__super__.render.apply(this, arguments);
    this.list.nestedSortable({
      toleranceElement: 'a',
      listType: 'ul',
      maxLevels: 2,
      stop: this.updateOrder
    });
    return this;
  };

  PagesMenu.prototype.updateOrder = function() {
    var data, items;

    items = this.list.nestedSortable('toHierarchy', {
      startDepthCount: 0,
      attribute: 'id',
      listType: 'ul'
    });
    data = {
      items: _.map(reorderPages(items, this.collection), JSON.stringify)
    };
    if (data.items.length > 0) {
      return $.post("" + (this.collection.url()) + "update_order/", data, function() {});
    }
  };

  PagesMenu.prototype.onAdd = function() {
    return this.dialog = createPageDialog('new', {
      callback: this.onAddResult
    });
  };

  PagesMenu.prototype.onAddResult = function(type, data) {
    var model,
      _this = this;

    model = new models.Page();
    return model.save(data, {
      success: function(model, response) {
        var _ref21;

        if ((_ref21 = _this.dialog) != null) {
          _ref21.hide().remove();
        }
        _this.collection.add(response.page);
        return router.navigate("pages/" + response.page.id, true);
      }
    });
  };

  return PagesMenu;

})(SidebarSection);

Sidebar = (function(_super) {
  __extends(Sidebar, _super);

  function Sidebar() {
    this.higlightPanorama = __bind(this.higlightPanorama, this);
    this.highlightPage = __bind(this.highlightPage, this);
    this.highlightAlbum = __bind(this.highlightAlbum, this);
    this.itemSelected = __bind(this.itemSelected, this);
    this.render = __bind(this.render, this);    _ref21 = Sidebar.__super__.constructor.apply(this, arguments);
    return _ref21;
  }

  Sidebar.prototype.initialize = function(_arg) {
    this.app = _arg.app;
    this.albums = new AlbumsSection({
      collection: this.app.data.albums
    });
    this.panoramas = new PanoramasSection({
      collection: this.app.data.panoramas
    });
    this.pages = new PagesMenu({
      collection: this.app.data.pages
    });
    this.albums.on('selected', this.itemSelected);
    this.panoramas.on('selected', this.itemSelected);
    this.pages.on('selected', this.itemSelected);
    return this.selectedView = null;
  };

  Sidebar.prototype.render = function() {
    this.$el.html('').append(this.albums.render().el).append(this.panoramas.render().el).append(this.pages.render().el);
    return this;
  };

  Sidebar.prototype.itemSelected = function(view, model) {
    console.log('item selected', model.appPath());
    return this.app.navigate(model.appPath(), {
      trigger: true
    });
  };

  Sidebar.prototype.highlightAlbum = function(id) {
    this.$('.active').removeClass('active');
    return this.albums.selectItem(id);
  };

  Sidebar.prototype.highlightPage = function(id) {
    this.$('.active').removeClass('active');
    return this.pages.selectItem(id);
  };

  Sidebar.prototype.higlightPanorama = function(id) {
    this.$('.active').removeClass('active');
    return this.panoramas.selectItem(id);
  };

  return Sidebar;

})(Backbone.View);

CollectionsManager = (function() {
  function CollectionsManager() {
    this.albums = new models.AlbumList();
    this.albums.loaded = false;
    this.panoramas = new models.PanoramaList();
    this.panoramas.loaded = false;
    this.pages = new models.PageList();
    this.pages.loaded = false;
    this.albums.fetch({
      success: this.collectionLoaded
    });
    this.panoramas.fetch({
      success: this.collectionLoaded
    });
    this.pages.fetch({
      success: this.collectionLoaded
    });
  }

  CollectionsManager.prototype.collectionLoaded = function(collection) {
    collection.loaded = true;
    return collection.trigger('loaded', collection);
  };

  return CollectionsManager;

})();

Router = (function(_super) {
  __extends(Router, _super);

  function Router() {
    _ref22 = Router.__super__.constructor.apply(this, arguments);
    return _ref22;
  }

  Router.prototype.routes = {
    '': 'index',
    'albums/:id': 'albums',
    'pages/:id': 'pages',
    'panoramas/:id': 'panoramas',
    'settings': 'settings'
  };

  Router.prototype.initialize = function() {
    var startHistory,
      _this = this;

    this.data = new CollectionsManager();
    this.history = Backbone.history;
    this.sidebar = new Sidebar({
      el: '#sidebar',
      app: this
    }).render();
    this.$mainEl = $('#main');
    this.mainEl = this.$mainEl[0];
    startHistory = function() {
      return _this.history.start({
        root: '/admin/',
        pushState: false
      });
    };
    return setTimeout(startHistory, 1);
  };

  Router.prototype.index = function() {
    return this.$mainEl.html('<div class="hero-unit"></div>');
  };

  Router.prototype.albums = function(album_id) {
    var openAlbum;

    openAlbum = function() {
      var editor, model;

      model = this.data.albums.get(album_id);
      editor = new AlbumEditor({
        model: model
      });
      this.$mainEl.html(editor.render().el);
      this.sidebar.highlightAlbum(album_id);
      return this.data.albums.off('loaded', openAlbum);
    };
    if (this.data.albums.loaded) {
      return openAlbum.call(this);
    } else {
      return this.data.albums.on('loaded', openAlbum, this);
    }
  };

  Router.prototype.pages = function(page_id) {
    var openPage;

    openPage = function() {
      var editor, el, model;

      model = this.data.pages.get(page_id);
      this.$mainEl.html(el = document.createElement('div'));
      editor = new PageEditor({
        model: model,
        el: el
      }).render();
      this.sidebar.highlightPage(page_id);
      return this.data.pages.off('loaded', openPage);
    };
    if (this.data.pages.loaded) {
      return openPage.call(this);
    } else {
      return this.data.pages.on('loaded', openPage, this);
    }
  };

  Router.prototype.panoramas = function(pano_id) {
    var openPanorama;

    openPanorama = function() {
      var editor, el, model;

      model = this.data.panoramas.get(pano_id);
      this.$mainEl.html(el = document.createElement('div'));
      editor = new PanoramaEditor({
        model: model,
        el: el
      }).render();
      this.sidebar.higlightPanorama(pano_id);
      return this.data.panoramas.off('loaded', openPanorama);
    };
    if (this.data.panoramas.loaded) {
      return openPanorama.call(this);
    } else {
      return this.data.panoramas.on('loaded', openPanorama, this);
    }
  };

  return Router;

})(Backbone.Router);

$(function() {
  return $.ajaxSetup({
    statusCode: {
      500: function() {
        return alert('Ошибка на сервере. Андрей опять что-то поломал?');
      },
      401: function() {
        return window.location = '/admin/login/';
      },
      404: function() {
        return alert('resource not found');
      },
      200: function(response) {
        var message;

        message = response.msg || 'Что-то пошло не так';
        if (!response.success) {
          return alert(message);
        }
      }
    }
  });
});
