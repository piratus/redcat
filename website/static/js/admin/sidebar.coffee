class SidebarSectionItem extends Backbone.View
  tagName: 'li'
  template: _.template '<a data-id="<%= id %>"><%= name %></a>'

  events:
    'click a': 'select'

  initialize: (options)->
    @parent = options.parent
    @model.on('change', @update)
    @model.on('destroy', @remove)

  render: ()=>
    @$el.html(@template(@model.toJSON()))
    this

  select: =>
    @parent.trigger('selected', this, @model)
    return false

  update: ()=>
    @render()

  remove: =>
    $(@el).hide(speed: 200).remove()


class SidebarSection extends Backbone.View
  className: 'sidebar-section'
  title: false

  headerTemplate: _.template """<p class="nav-header">
                             <%= title %><i class="icon-plus add"></i></p>"""

  itemClass: SidebarSectionItem

  events:
    'click .add': 'onAdd'

  initialize: ->
    @collection.on('add', @addItem)
    @list = $('<ul class="nav nav-list">')

  render: ->
    @el.innerHTML = if @title then @headerTemplate(title: @title) else ''
    @list.html('').appendTo(@el)
    @collection.each(@addItem)
    this

  addItem: (item)=>
    itemView = new @itemClass(model: item, parent: this)
    @list.append(itemView.render().el)

  onAdd: ->

  selectItem: (id)->
    @list.find("[data-id=#{id}]").closest('li').addClass('active')


class AlbumsSection extends SidebarSection
  title: 'Альбомы'

  render: ->
    super
    @list.sortable(handle: 'a', stop: @updateOrder)
    this

  updateOrder: =>
    $.post "/admin/api/albums/update_order/", reorder(@$('ul a'), @collection)

  onAdd: =>
    @dialog = createAlbumDialog 'new', callback: @onAddResult

  onAddResult: (type, data)=>
    model = new models.Album()
    model.save data, success: (model, response)=>
      @dialog?.hide().remove()
      @collection.add(model)

class PanoramasSection extends SidebarSection
  title: 'Панорамы'

  onAdd: ->
    @dialog = createPanoramaDialog 'new', callback: @onAddResult

  onAddResult: (type, data)=>
    model = new models.Panorama()
    model.save data, success: (model, response) =>
      if response.success is true
        @collection.add([response.panorama])
      else
        alert(response.message or 'Что-то пошло не так')
      @dialog?.hide().remove()


class PagesMenuItem extends SidebarSectionItem
  template: _.template '<a data-id="<%= id %>"><%= menu_title %></a><ul></ul>'

  initialize: (options)->
    super options
    @children = options.children or []

  render: ->
    super
    @$el.attr('id', "page-#{@model.get('id')}")

    if @children?.length > 0
      childList = @$('ul')
      for child in @children
        view = new PagesMenuItem(model: child, parent: @parent, children: [])
        childList.append(view.render().el)
    this

  update: =>
    @$('a').first().replaceWith(@template(@model.toJSON()))
    this


class PagesMenu extends SidebarSection
  title: 'Страницы'
  itemClass: PagesMenuItem

  render: ->
    @list.collection = @collection.toTree()

    super

    @list.nestedSortable
      toleranceElement: 'a'
      listType: 'ul'
      maxLevels: 2
      stop: @updateOrder

    this

  updateOrder: =>
    items = @list.nestedSortable('toHierarchy',
                                 startDepthCount: 0,
                                 attribute: 'id',
                                 listType: 'ul')
    data = {items: _.map(reorderPages(items, @collection), JSON.stringify)}

    if data.items.length > 0
      $.post "#{@collection.url()}update_order/", data, ->

  onAdd: =>
    @dialog = createPageDialog 'new', callback: @onAddResult

  onAddResult: (type, data)=>
    model = new models.Page()
    model.save data, success: (model, response)=>
      @dialog?.hide().remove()
      @collection.add(response.page)
      router.navigate("pages/#{response.page.id}", true)


class Sidebar extends Backbone.View
  initialize: ({@app})->
    @albums = new AlbumsSection(collection: @app.data.albums)
    @panoramas = new PanoramasSection(collection: @app.data.panoramas)
    @pages = new PagesMenu(collection: @app.data.pages)

    @albums.on('selected', @itemSelected)
    @panoramas.on('selected', @itemSelected)
    @pages.on('selected', @itemSelected)

    @selectedView = null

  render: =>
    @$el
      .html('')
      .append(@albums.render().el)
      .append(@panoramas.render().el)
      .append(@pages.render().el)
    this

  itemSelected: (view, model)=>
    console.log('item selected', model.appPath())
    @app.navigate(model.appPath(), trigger: true)

  highlightAlbum: (id)=>
    @$('.active').removeClass('active')
    @albums.selectItem(id)

  highlightPage: (id)=>
    @$('.active').removeClass('active')
    @pages.selectItem(id)

  higlightPanorama: (id)=>
    @$('.active').removeClass('active')
    @panoramas.selectItem(id)
