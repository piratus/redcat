#
# LIBRARY FUNCTIONS
#
namespace = (path)=>
    root = this
    for name in path.split()
        root[name] ?= {}
        root = root[name]
    root

reorder = (elements, collection)->
    result = {}
    for item, index in elements
        order = index + 1
        model = collection.get($(item).data('id'))
        if model.get('order') isnt index + 1
            model.set({order: order}, silent: true)
            result[model.id] = index + 1
    result

newID = (->
    counter = 1
    closure = -> "rc-#{counter++}"
)()

#
# FORMS
#
class Form
    defaults:
        fields: []

    fieldDefaults:
        type: 'text'
        cls: 'span5'

    constructor: (options)->
        @options = _.extend({}, @defaults, options)
        @el = document.createElement('form')
        @$el = $(@el)
        @fields = (@_createField(_.extend({}, @fieldDefaults, field)) for field in @options.fields)

    render: =>
        @$el.append(field.render().el) for field in @fields

        if @options.values
            @setValues(@options.values)

        this

    values: =>
        values = {}
        for field in @fields
            values[field.name] = field.getValue()
        values

    setValues: (values)=>
        for field in @fields
            value = values[field.name]
            if value?
                field.setValue(value)
        this

    _createField: (config)->
        config = _.extend({}, @fieldDefaults, config)
        switch config.type
            when 'text' then new TextField(config)
            when 'textarea' then new TextArea(config)

class TextField
    template: _.template("""<label for="<%= id %>"><%= label %></label>
                <input id="<%= id %>" class="<%= cls %>" name="<%= name %>" type="<%= type %>" value="<%= value %>">
        """)

    defaults:
        cls: ''
        value: ''

    constructor: (options)->
        @options = _.extend({}, @defaults, options)
        @el = document.createElement('div')

        @id = @options.id ?= newID()
        @name = @options.name ?= @options.id

        @options.label ?= @options.name

    render: =>
        $(@el).addClass('clearfix').html(@template(@options))
        this

    getValue: ()=>
        $('#' + @options.id, @el).val()

    setValue: (value)=>
        $('#' + @options.id, @el).val(value)

class TextArea extends TextField
    template: _.template("""
        <label for="<%= id %>"><%= label %></label>
        <textarea id="<%= id %>" class="<%= cls %>" name="<%= name %>"><%= value %></textarea>
    """)

#
# Dialogs
#
class Dialog
    template: _.template("""
        <div class="modal-header"><a href="#" class="close" data-dismiss="modal">×</a><h3><%= title %></h3></div>
        <div class="modal-body"><%= content %></div>
        <div class="modal-footer"></div>""")

    defaults:
        title: ''
        content: ''
        show: true
        keyboard: true
        backdrop: true
        buttons: []
        cls: ''

    constructor: (options)->
        @options = _.extend({}, @defaults, options)

        $(@el = document.createElement('div'))
            .addClass("modal hide #{@options.cls}")
            .html(@template(@options))
            .modal(
                show: @options.show
                keyboard: @options.keyboard
                backdrop: @options.backdrop
            )

        @titleEl = $('.modal-header h3', @el)
        @contentEl = $('.modal-body', @el)
        @footerEl = $('.modal-footer', @el)

        self = this
        handler = ->
            action = $(@).data('action')
            self._click(action)

        for button in @options.buttons
            $(@_createButton(button))
                .click(handler)
                .appendTo(@footerEl)

        @footerEl.hide() if @options.buttons.length == 0

    show: =>
        $(@el).modal('show')

    hide: =>
        $(@el).modal('hide')

    _click: (type)=>
        if @options.callback
            @options.callback.call(this, type)

    _createButton: (type)->
        switch type
            when 'save'
                '<span class="btn btn-primary" data-action="save">Сохранить</span>'
            when 'delete'
                '<span class="btn btn-danger pull-left" data-action="delete">Удалить</span>'


class FormDialog extends Dialog
    constructor: (options)->
        options.content = ''
        options.cls += ' form-dialog'
        super options

        @form = new Form(@options.form)
        @contentEl.html(@form.render().el)

    _click: (type)=>
        if @options.callback
            @options.callback.call(this, type, @form.values())
