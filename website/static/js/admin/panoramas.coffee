createPanoramaDialog = (type, {values, callback})->
  config =
    title: 'Новая панорама'
    buttons: ['save']
    form:
      fields: [name: 'name', label: 'Название']
      values: values or {}

  if type is 'edit'
    _.extend config,
      title: "Панорама «#{values.name}»"
      buttons: ['delete', 'save']

  if callback?
    _.extend config, callback: callback

  return new FormDialog(config)


class FileField extends Backbone.View
  template: _.template """
    <label class="control-label"><%= label %></label>
    <div class="controls">
      <input type="file" class="fileinput" accept="<%= accept %>"/>
      <div class="display <%= displayClass %>"></div>
      <a class="btn btn-primary upload" href="#">Загрузить</a>
    </div>
  """

  className: 'control-group'
  label: 'field'
  displayClass: ''
  accept: 'application/x-shockwave-flash'

  successTemplate: _.template('<i class="icon icon-check"></i> <span><%= value %></span>')
  errorTemplate: _.template('<i class="icon icon-remove"></i> <span><%= message %></span>')

  events:
    'click .upload': 'selectUpload'

  initialize: (options)->
    @label = options.label if options.label
    @displayClass = options.displayClass if options.displayClass
    @fileType = options.type if options.type
    @fieldName = options.field if options.field

  render: ->
    @$el.html(@template(this))

    value = @model.get(@fieldName)
    displayedValue =
      if value
        @successTemplate(value: value)
      else
        @errorTemplate(message: 'файл не загружен')

    @display = @$el.find('.display').html(displayedValue)

    @$('input[type=file]').fileupload
      url: @model.url()
      dataType: 'json'
      paramName: @fileType
      progressall: @progressHandler
      done: @uploadFinished
      type: 'PUT'

    this

  uploadFinished: (e, data)=>
    {success, panorama, message} = data.result
    if success is true
      @model.set(panorama)
      @display.html(@successTemplate(value: panorama[@fieldName]))
    else
      @model.set(@fieldName, false)
      @display.html(@errorTemplate(message: message ? 'файл не загружен'))

  selectUpload: (event)->
    @$('input[type=file]').trigger('click')
    event.preventDefault()

  progressHandler: (e, data)=>
    if not @$('.progress').length
      @display.html('<div class="progress" style="width: 100%;"><div class="bar" style="width: 0;"></div></div>')

    progress = parseInt(data.loaded / data.total * 100, 10)
    @$('.bar').css('width', progress + '%')


class ImageField extends FileField
  label: 'Картинка'
  displayClass: 'preview'
  fileType: 'image'
  fieldName: 'preview_image'
  accept: 'image/jpeg'
  successTemplate: _.template('<img src="<%= value %>?<%= (new Date()).getTime() %>">')


class PanoramaEditor extends Backbone.View
  template: _.template """
    <h3 class="page-title"><%= name %></h3>
    <div class="actions">
      <div class="btn-group">
        <span class="btn edit">Параметры</span>
      </div>
      <div class="btn-group">
        <span class="btn publish">Опубликовать</span>
      </div>
    </div>
    <form class="form-horizontal panorama-form"></form>
  """

  className: 'hero-unit'

  events:
    'click .preview': 'togglePreview'
    'click .edit': 'edit'
    'click .publish': 'publish'

  initialize: ->
    @model.bind('change:name', @updateName, this)
    @model.bind('change:published', @updatePublished, this)
    @model.bind('destroy', @destroy)

  render: ->
    @$el.html(@template(@model.toJSON())).addClass(@className)

    @$el.find('form')
      .append((new ImageField(model: @model)).render().el)
      .append((new FileField(model: @model, label: 'Высокое качество', type: 'hq', field: 'hq_file_size')).render().el)
      .append((new FileField(model: @model, label: 'Среднее качество', type: 'mq', field: 'mq_file_size')).render().el)

    @$publishBtn = @$('.publish').tooltip()
    @updatePublished()

    this

  togglePreview: (event)->
    $(event.currentTarget).toggleClass('expanded')

  publish: ->
    @model.togglePublished()

  updatePublished: ->
    published = @model.get('published')
    @$publishBtn.removeClass('btn-success btn-danger')
      .text(if published then 'Опубликована' else 'Скрыта')
      .addClass(if published then 'btn-success' else 'btn-danger')
    this

  updateName: ->
    @$('.page-title').html(@model.get('name'))

  edit: ->
    dialog = createPanoramaDialog 'edit',
      values: @model.toJSON()
      callback: (type, data)=>
        switch type
          when 'save'
            @model.save data, success: -> dialog.hide().remove()
          when 'delete'
            if @model.get('published') is false
              @model.destroy success: -> dialog.hide().remove()

  destroy: =>
    @$el.html('')
    router?.navigate('', trigger: true)
