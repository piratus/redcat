# Get form values to a javascript object
jQuery.fn.serializeObject = ->
    arrayData = @serializeArray()
    objectData = {}

    $.each arrayData, ->
        if @value?
            value = @value
        else
            value = ''

        if objectData[@name]?
            unless objectData[@name].push
                objectData[@name] = [objectData[@name]]

            objectData[@name].push value
        else
            objectData[@name] = value

    return objectData

# Markitup settings
MARKITUP_BUTTONS = [
    {
        name:'Bold'
        key:'B'
        openWith:'(!(<strong>|!|<b>)!)'
        closeWith:'(!(</strong>|!|</b>)!)'
    }
    {
        name:'Italic'
        key:'I'
        openWith:'(!(<em>|!|<i>)!)'
        closeWith:'(!(</em>|!|</i>)!)'
    }
    {
        name:'Stroke through'
        key:'S', openWith:'<del>'
        closeWith:'</del>'
    }
    {separator:'---------------' }
    {
        name:'Bulleted List'
        openWith:'    <li>'
        closeWith:'</li>'
        multiline:true
        openBlockWith:'<ul>\n'
        closeBlockWith:'\n</ul>'
    }
    {
        name:'Numeric List'
        openWith:'    <li>'
        closeWith:'</li>'
        multiline:true
        openBlockWith:'<ol>\n'
        closeBlockWith:'\n</ol>'
    }
    {separator:'---------------' }
    {
        name:'Picture'
        key:'P'
        replaceWith:'<img src="[![Source:!:http://]!]" alt="[![Alternative text]!]" />'
    }
    {
        name:'Link'
        key:'L'
        openWith:'<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>'
        closeWith:'</a>'
        placeHolder:'Your text to link...'
    }
    {separator:'---------------' }
    {
        name:'Clean'
        className:'clean'
        replaceWith: (markitup)->
            markitup.selection.replace(/<(.*?)>/g, '')
    }
    {
        name:'Preview'
        className:'preview'
        call:'preview'
    }
]


@MARKITUP_SETTINGS =
    onShiftEnter:
        keepDefault: false
        replaceWith: '<br />\n'
    onCtrlEnter:
        keepDefault: false
        openWith: '\n<p>'
        closeWith: '</p>'
    onTab:
        keepDefault: false
        replaceWith: '    '
    markupSet: MARKITUP_BUTTONS
    previewInWindow: 'width=800, height=600, resizable=yes, scrollbars=yes'
