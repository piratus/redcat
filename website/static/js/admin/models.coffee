@models ?= {}

class @models.Album extends Backbone.Model
  url: =>
    id = @get('id')
    if id
      "/admin/api/albums/#{id}/"
    else
      "/admin/api/albums/"

  appPath: =>
    "/albums/#{@id}"

  parse: (response)->
    response.album or response

class @models.AlbumList extends Backbone.Collection
  model: models.Album

  url: ->
    '/admin/api/albums/'

  comparator: (album) ->
    album.get 'order'

  parse: (response)->
    response.albums

class @models.Photo extends Backbone.Model
  url: ->
    "/admin/api/photos/#{@get('id')}/"

  thumb: ->
    "#{@get('url')}?size=100"

class @models.PhotoList extends Backbone.Collection
  model: models.Photo

  constructor: (@albumId)->
    throw "Album ID is not specified" if not @albumId?
    super

  url: ->
    "/admin/api/albums/#{@albumId}/"

  comparator: (photo)->
    photo.get 'order'

  parse: (response)->
    response.photos

class @models.Panorama extends Backbone.Model
  url: =>
    id = @get('id')
    if id
      "/admin/api/panoramas/#{id}/"
    else
      "/admin/api/panoramas/"

  appPath: =>
    "/panoramas/#{@id}"

  parse: (response)->
    response.panorama or response

  togglePublished: (options={})->
    data = published: not @get('published')
    @save(data, options)


class @models.PanoramaList extends Backbone.Collection
  model: models.Panorama

  url: ->
    '/admin/api/panoramas/'

  parse: (response)->
    response.panoramas

class @models.Page extends Backbone.Model
  defaults:
    title: ''
    menu_title: ''
    slug: ''
    content: ''
    description: ''
    keywords: ''
    published: false

  url: ->
    if id = @get('id')
      "/admin/api/pages/#{id}/"
    else
      '/admin/api/pages/'

  appPath: =>
    "/pages/#{@id}"

  parse: (response)->
    response.page or response

  togglePublished: (options={})->
    data = published: not @get('published')
    @save(data, options)

class @models.PageList extends Backbone.Collection
  model: models.Page

  url: ->
    '/admin/api/pages/'

  parse: (response)->
    response.pages

  toTree: ->
    rootItems = @filter (item)-> item.get('parent_id') is null
    tree = new Backbone.Collection(rootItems)
    for rootItem in rootItems
      rootItem.children = @filter (item)->
        item.get('parent_id') is rootItem.id
    tree
