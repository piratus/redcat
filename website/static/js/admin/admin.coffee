class CollectionsManager
  constructor: ->
    @albums = new models.AlbumList()
    @albums.loaded = false
    @panoramas = new models.PanoramaList()
    @panoramas.loaded = false
    @pages = new models.PageList()
    @pages.loaded = false

    @albums.fetch(success: @collectionLoaded)
    @panoramas.fetch(success: @collectionLoaded)
    @pages.fetch(success: @collectionLoaded)

  collectionLoaded: (collection)->
    collection.loaded = true
    collection.trigger('loaded', collection)


class Router extends Backbone.Router
  routes:
    '': 'index'
    'albums/:id': 'albums'
    'pages/:id': 'pages'
    'panoramas/:id': 'panoramas'
    'settings': 'settings'

  initialize: ->
    @data = new CollectionsManager()
    @history = Backbone.history
    @sidebar = new Sidebar(el: '#sidebar', app: this).render()
    @$mainEl = $('#main')
    @mainEl = @$mainEl[0]

    startHistory = =>
      @history.start(root: '/admin/', pushState: false)
    setTimeout(startHistory, 1)

  index: ->
    @$mainEl.html('<div class="hero-unit"></div>')

  albums: (album_id)->
    openAlbum = ->
      model = @data.albums.get(album_id)
      editor = new AlbumEditor(model: model)
      @$mainEl.html(editor.render().el)
      @sidebar.highlightAlbum(album_id)
      @data.albums.off('loaded', openAlbum)

    if @data.albums.loaded
      openAlbum.call(this)
    else
      @data.albums.on('loaded', openAlbum, this)

  pages: (page_id)->
    openPage = ->
      model = @data.pages.get(page_id)
      @$mainEl.html(el = document.createElement('div'))
      editor = new PageEditor(model: model, el: el).render()
      @sidebar.highlightPage(page_id)
      @data.pages.off('loaded', openPage)

    if @data.pages.loaded
      openPage.call(this)
    else
      @data.pages.on('loaded', openPage, this)

  panoramas: (pano_id)->
    openPanorama = ->
      model = @data.panoramas.get(pano_id)
      @$mainEl.html(el = document.createElement('div'))
      editor = new PanoramaEditor(model: model, el: el).render()
      @sidebar.higlightPanorama(pano_id)
      @data.panoramas.off('loaded', openPanorama)

    if @data.panoramas.loaded
      openPanorama.call(this)
    else
      @data.panoramas.on('loaded', openPanorama, this)

$ ->
  $.ajaxSetup
    statusCode:
      500: ->
        alert 'Ошибка на сервере. Андрей опять что-то поломал?'
      401: ->
        window.location = '/admin/login/'
      404: ->
        alert 'resource not found'
      200: (response)->
        message = response.msg or 'Что-то пошло не так'
        alert message if not response.success
