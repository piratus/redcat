createAlbumDialog = (type, {values, callback})->
  config =
    title: 'Новый альбом'
    buttons: ['save']
    form:
      fields: [
        {name: 'name', label: 'Название'}
        {name: 'slug', label: 'Путь в URL'}
      ]
      values: values or {}
  if type is 'edit'
    _.extend config,
      title: "Альбом «#{values.name}»"
      buttons: ['delete', 'save']
  if callback?
    _.extend config, callback: callback
  return new FormDialog(config)


class AlbumEditorItem extends Backbone.View
  tagName: 'li'
  template: _.template """
      <a data-id="<%= id %>" class="thumbnail">
        <img height="100" width="100" src="<%= url %>?size=100">
        <i class="icon-remove"></i>
      </a>
    """

  events:
    'click .icon-remove': 'remove'

  render: ()=>
    @$el.html(@template(@model.toJSON()))
    this

  remove: =>
    @model.destroy()


class AlbumEditor extends Backbone.View
  template: _.template """
       <h3 class="page-title"><%= name %></h3>
       <div class="actions">
         <span class="btn edit">Параметры</span>
       </div>
       <div class="progress"  style="display: none;"><span class="bar"></span></div>
       <div class="dropzone centered">
         <i class="icon-download-alt"></i> файлы бросать сюда
         <input type="file" class="fileinput">
       </div>
       <ul class="thumbnails"></ul>
     """
  events:
    'click .edit': 'edit'

  className: 'hero-unit'
  itemClass: AlbumEditorItem

  initialize: ->
    @albumId = @model.id

    @model.on('change:name', @updateName)
    @model.on('destroy', @destroy)

    @collection = new models.PhotoList(@albumId)
    @collection.on('reset', @render)
    @collection.on('add', @renderItem)

  render: ->
    @$el.html(@template(@model.toJSON())).addClass(@className)

    @$('.thumbnails').sortable
      stop: @updateOrder
      handle: 'img'

    $('body').on 'keydown', (event)=>
      @$el.addClass('deleteMode') if event.altKey
    $('body').on 'keyup', (event)=>
      @$el.removeClass('deleteMode') unless event.altKey

    @initFileUpload()

    @collection.fetch()

    this

  renderItem: (model)=>
    view = new @itemClass(model: model, parent: this)
    @$('.thumbnails').append(view.render().el)

  initFileUpload: ->
    @$('input[type=file]').fileupload
      url: '/admin/api/photos/'
      dataType: 'json'
      paramName: 'photo'
      formData:
        album_id: @albumId
      dropZone: @$el
      sequentialUploads: true
      limitConcurrentUploads: 1
      progressall: @progressHandler
      done: @uploadFinished

  updateName: =>
    @$('.page-title').html(@model.get('name'))

  updateOrder: =>
    $.post "/admin/api/albums/#{@albumId}/update_order/", reorder(@$('ul a'), @collection)

  progressHandler: (e, data)=>
    @$('.progress').show()

    progress = parseInt(data.loaded / data.total * 100, 10)
    @$('.bar').css('width', progress + '%')

  uploadFinished: (e, data)=>
    @$('.progress').hide()
    {success, photo, message} = data.result
    if success is true
      @collection.add([photo])
    else
      alert(message or 'Что-то пошло не так')

  edit: ->
    dialog = createAlbumDialog 'edit',
      values: @model.toJSON()
      callback: (type, data)=>
        switch type
          when 'save'
            @model.save data, success: -> dialog.hide().remove()
          when 'delete'
            if confirm 'Удалить альбом?'
              @model.destroy success: -> dialog.hide().remove()

  destroy: =>
    @$el.html('')
    router?.navigate('', trigger: true)
