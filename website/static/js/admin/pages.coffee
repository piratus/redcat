createPageDialog = (type, {values, callback})->
    config =
        title: 'Новая страница'
        buttons: ['save']
        form:
            fields: [
                {name: 'title', label: 'Заголовок'}
                {name: 'menu_title', label: 'Заголовок в меню'}
                {name: 'slug', label: 'Путь в URL'}
            ]
            values: values
        callback: callback
    if type is 'edit'
        _.extend config,
            title: "Cтраница «#{values.menu_title}»"
            buttons: ['delete', 'save']
        config.form.fields = _.union config.form.fields, [
                    {name: 'description', type: 'textarea', label: 'Description'}
                    {name: 'keywords', type: 'textarea', label: 'Keywords'}
                ]
    new FormDialog(config)

reorderPages = (items, collection)->
    result = []
    for item, order in items
        data = order: order, parent_id: null
        model = collection.get(item.id)
        if model.get('parent_id') isnt null or model.get('order') isnt order
            model.set(data, silent: yes)
            result.push _.extend(data, id: item.id)

        for subItem, subOrder in item.children or []
            data = order: subOrder, parent_id: item.id
            model = collection.get(subItem.id)
            if model.get('parent_id') isnt item.id or model.get('order') isnt subOrder
                model.set(data, silent: yes)
                result.push _.extend(data, id: subItem.id)
    result


class PageEditor extends Backbone.View
    template: _.template """
    <h3 class="page-title">
        <span><%= menu_title %></span>&nbsp;
    </h3>
    <div class="actions">
      <div class="btn-group">
        <span class="btn edit">Параметры</span>
        <span class="btn btn-primary save">Сохранить</span>
      </div>
      <div class="btn-group">
        <span class="btn publish">Опубликовать</span>
      </div>
    </div>
    <form><textarea id="page-content" style="width: 98%" rows="20"><%= content %></textarea></form>
    """

    className: 'hero-unit'

    events:
        'click .edit': 'edit'
        'click .save': 'save'
        'click .publish': 'publish'

    codeMirrorSettings:
        mode: 'text/html'
        tabMode: 'indent'
        lineNumbers: true


    initialize: ()->
        @model.bind('change:menu_title', @updateTitle, this)
        @model.bind('change:published', @updatePublished, this)
        @model.bind('destroy', @destroy)

    render: =>
        @$el.html(@template(@model.toJSON())).addClass(@className)
        @editor = CodeMirror.fromTextArea(@$('textarea')[0], @codeMirrorSettings)
        @$publishBtn = @$('.publish')
        @updatePublished()
        @$publishBtn.tooltip()
        this

    updateTitle: ->
        @$('.page-title > span').html(@model.get('title'))

    updatePublished: ->
        published = @model.get('published')
        @$publishBtn.removeClass('btn-success btn-danger')
            .text(if published then 'Опубликована' else 'Скрыта')
            .addClass(if published then 'btn-success' else 'btn-danger')
        this

    destroy: =>
        $(@el).html('')

    edit: ->
        model = @model
        createPageDialog 'edit',
            values: @model.toJSON()
            callback: (type, data)->
                switch type
                    when 'save'
                        model.save data, success: => @hide().remove()
                    when 'delete'
                        if model.get('published') is false
                            model.destroy success: => @hide().remove()
                        else
                            alert "Нельзя удалять опубликованное!"

    save: =>
        @model.save content: @editor.getValue()
        true

    publish: ->
        @model.togglePublished()

