# coding: utf-8
from flask.ext.assets import Environment, Bundle

assets = Environment()

assets.register('galleria_js',
                Bundle(
                    'js/libs/galleria.js',
                    'js/libs/themes/folio/galleria.folio.min.js',
                    'js/libs/galleria.history.js',
                    output='js/galleria.all.js',
                    filters=['closure_js']
                ))

assets.register('admin_libs',
                Bundle(
                    'js/libs/codemirror/codemirror.js',
                    'js/libs/codemirror/xml.js',
                    'js/libs/codemirror/css.js',
                    'js/libs/codemirror/javascript.js',
                    'js/libs/codemirror/htmlmixed.js',
                    'js/libs/jquery-ui.min.js',
                    'js/libs/jquery.mjs.nestedSortable.js',
                    'js/libs/jquery.fileupload.js',
                    'js/libs/json2.js',
                    'js/libs/underscore.js',
                    'js/libs/backbone.js',
                    'js/libs/bootstrap/bootstrap-modal.js',
                    'js/libs/bootstrap/bootstrap-tooltip.js',
                    'js/libs/bootstrap/bootstrap-popover.js',
                    output='js/admin.libs.js',
                    filters=['closure_js']
                ))

assets.register('admin_js',
                Bundle(
                    'js/admin/utils.coffee',
                    'js/admin/components.coffee',
                    'js/admin/models.coffee',
                    'js/admin/albums.coffee',
                    'js/admin/panoramas.coffee',
                    'js/admin/pages.coffee',
                    'js/admin/sidebar.coffee',
                    'js/admin/admin.coffee',
                    output='js/admin.js',
                    filters=['coffeescript']
                ))

assets.register('style_css',
                Bundle(
                    'style.less',
                    output='style.css',
                    filters=['less', 'yui_css']
                ))

assets.register('admin_css',
                Bundle(
                    'admin.less',
                    'js/libs/codemirror/codemirror.css',
                    output='admin.all.css',
                    filters=['less']
                ))
