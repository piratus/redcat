from flask.ext.script import Manager
from flask.ext.assets import ManageAssets
from website import app, db, assets


manager = Manager(app)
manager.add_command('assets', ManageAssets(assets))


@manager.command
def create_db():
    """Create database"""
    db.create_all()


if __name__ == '__main__':
    manager.run()
