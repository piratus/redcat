# RedCat Studio web site

## Deployment

### httpd.conf

    WSGIDaemonProcess redcat processes=1 python-path=/home/piratus/webapps/redcat/lib/python2.7:/home/piratus/webapps/redcat/redcat threads=1
    WSGIProcessGroup redcat
    WSGIRestrictEmbedded On
    WSGILazyInitialization On

    WSGIPythonPath /home/piratus/webapps/redcat/htdocs/
    WSGIScriptAlias / /home/piratus/webapps/redcat/htdocs/index.py

    <Directory /home/piratus/webapps/redcat/htdocs/>
        AddHandler wsgi-script .py
        RewriteEngine on
        RewriteBase /
        WSGIScriptReloading On
    </Directory>

### main.py

    import os

    activate_this = os.path.expanduser("~/.virtualenvs/redcat/bin/activate_this.py")
    execfile(activate_this, dict(__file__=activate_this))

    from website import app as application

Looks like the *activate_this* bit is not needed if you set up *WSGIPythonPath* to point to virtualenv lib dir. More info http://code.google.com/p/modwsgi/wiki/VirtualEnvironments

To serve application from non-root URL a middleware is needed. It's ok to put it in main.py

    class WebFactionMiddleware(object):
        def __init__(self, app):
            self.app = app
        def __call__(self, environ, start_response):
            environ['SCRIPT_NAME'] = '/p/redcat'
            return self.app(environ, start_response)

    application.wsgi_app = WebFactionMiddleware(application.wsgi_app)
